﻿namespace Chickenfork_POS
{
    partial class Admin_Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Admin_Home));
            this.pnContainer = new System.Windows.Forms.Panel();
            this.flpInfoContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.gbStatistikPenjualan = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbMinumanTerjual = new System.Windows.Forms.Label();
            this.lbColon1 = new System.Windows.Forms.Label();
            this.lbPendapatan = new System.Windows.Forms.Label();
            this.lbJumlahTransaksi = new System.Windows.Forms.Label();
            this.lbMakananTerjual = new System.Windows.Forms.Label();
            this.lbColon4 = new System.Windows.Forms.Label();
            this.lbColon2 = new System.Windows.Forms.Label();
            this.lbColon3 = new System.Windows.Forms.Label();
            this.lbAngkaPendapatan = new System.Windows.Forms.Label();
            this.lbAngkaJumlahTransaksi = new System.Windows.Forms.Label();
            this.lbAngkaMakananTerjual = new System.Windows.Forms.Label();
            this.lbAngkaMinumanTerjual = new System.Windows.Forms.Label();
            this.gbMenuMakanan = new System.Windows.Forms.GroupBox();
            this.flpDaftarMenuMakanan = new System.Windows.Forms.FlowLayoutPanel();
            this.gbMenuMinuman = new System.Windows.Forms.GroupBox();
            this.flpDaftarMenuMinuman = new System.Windows.Forms.FlowLayoutPanel();
            this.gbDaftarStaff = new System.Windows.Forms.GroupBox();
            this.flpDaftarStaff = new System.Windows.Forms.FlowLayoutPanel();
            this.lbKeterangan = new System.Windows.Forms.Label();
            this.lbSelamatDatang = new System.Windows.Forms.Label();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.pnContainer.SuspendLayout();
            this.flpInfoContainer.SuspendLayout();
            this.gbStatistikPenjualan.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbMenuMakanan.SuspendLayout();
            this.gbMenuMinuman.SuspendLayout();
            this.gbDaftarStaff.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            this.pnContainer.Controls.Add(this.flpInfoContainer);
            this.pnContainer.Controls.Add(this.lbKeterangan);
            this.pnContainer.Controls.Add(this.lbSelamatDatang);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(30, 30);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.Size = new System.Drawing.Size(1306, 667);
            this.pnContainer.TabIndex = 11;
            // 
            // flpInfoContainer
            // 
            this.flpInfoContainer.AutoScroll = true;
            this.flpInfoContainer.Controls.Add(this.gbStatistikPenjualan);
            this.flpInfoContainer.Controls.Add(this.gbMenuMakanan);
            this.flpInfoContainer.Controls.Add(this.gbMenuMinuman);
            this.flpInfoContainer.Controls.Add(this.gbDaftarStaff);
            this.flpInfoContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpInfoContainer.Location = new System.Drawing.Point(0, 186);
            this.flpInfoContainer.Margin = new System.Windows.Forms.Padding(3, 50, 3, 3);
            this.flpInfoContainer.Name = "flpInfoContainer";
            this.flpInfoContainer.Padding = new System.Windows.Forms.Padding(0, 0, 0, 40);
            this.flpInfoContainer.Size = new System.Drawing.Size(1306, 481);
            this.flpInfoContainer.TabIndex = 10;
            // 
            // gbStatistikPenjualan
            // 
            this.gbStatistikPenjualan.Controls.Add(this.tableLayoutPanel1);
            this.gbStatistikPenjualan.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbStatistikPenjualan.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbStatistikPenjualan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.gbStatistikPenjualan.Location = new System.Drawing.Point(3, 3);
            this.gbStatistikPenjualan.Name = "gbStatistikPenjualan";
            this.gbStatistikPenjualan.Padding = new System.Windows.Forms.Padding(10);
            this.gbStatistikPenjualan.Size = new System.Drawing.Size(1240, 255);
            this.gbStatistikPenjualan.TabIndex = 0;
            this.gbStatistikPenjualan.TabStop = false;
            this.gbStatistikPenjualan.Text = "Statistik Penjualan";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.78014F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.219858F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 961F));
            this.tableLayoutPanel1.Controls.Add(this.lbMinumanTerjual, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbColon1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbPendapatan, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbJumlahTransaksi, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbMakananTerjual, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbColon4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbColon2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbColon3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbAngkaPendapatan, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbAngkaJumlahTransaksi, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbAngkaMakananTerjual, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbAngkaMinumanTerjual, 2, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 45);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1220, 200);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lbMinumanTerjual
            // 
            this.lbMinumanTerjual.AutoSize = true;
            this.lbMinumanTerjual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbMinumanTerjual.Location = new System.Drawing.Point(3, 150);
            this.lbMinumanTerjual.Name = "lbMinumanTerjual";
            this.lbMinumanTerjual.Size = new System.Drawing.Size(213, 35);
            this.lbMinumanTerjual.TabIndex = 11;
            this.lbMinumanTerjual.Text = "Minuman Terjual";
            // 
            // lbColon1
            // 
            this.lbColon1.AutoSize = true;
            this.lbColon1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbColon1.Location = new System.Drawing.Point(238, 0);
            this.lbColon1.Name = "lbColon1";
            this.lbColon1.Size = new System.Drawing.Size(17, 35);
            this.lbColon1.TabIndex = 4;
            this.lbColon1.Text = ":";
            // 
            // lbPendapatan
            // 
            this.lbPendapatan.AutoSize = true;
            this.lbPendapatan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbPendapatan.Location = new System.Drawing.Point(3, 0);
            this.lbPendapatan.Name = "lbPendapatan";
            this.lbPendapatan.Size = new System.Drawing.Size(156, 35);
            this.lbPendapatan.TabIndex = 0;
            this.lbPendapatan.Text = "Pendapatan";
            // 
            // lbJumlahTransaksi
            // 
            this.lbJumlahTransaksi.AutoSize = true;
            this.lbJumlahTransaksi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbJumlahTransaksi.Location = new System.Drawing.Point(3, 51);
            this.lbJumlahTransaksi.Name = "lbJumlahTransaksi";
            this.lbJumlahTransaksi.Size = new System.Drawing.Size(211, 35);
            this.lbJumlahTransaksi.TabIndex = 1;
            this.lbJumlahTransaksi.Text = "Jumlah Transaksi";
            // 
            // lbMakananTerjual
            // 
            this.lbMakananTerjual.AutoSize = true;
            this.lbMakananTerjual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbMakananTerjual.Location = new System.Drawing.Point(3, 102);
            this.lbMakananTerjual.Name = "lbMakananTerjual";
            this.lbMakananTerjual.Size = new System.Drawing.Size(209, 35);
            this.lbMakananTerjual.TabIndex = 2;
            this.lbMakananTerjual.Text = "Makanan Terjual";
            // 
            // lbColon4
            // 
            this.lbColon4.AutoSize = true;
            this.lbColon4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbColon4.Location = new System.Drawing.Point(238, 150);
            this.lbColon4.Name = "lbColon4";
            this.lbColon4.Size = new System.Drawing.Size(17, 35);
            this.lbColon4.TabIndex = 3;
            this.lbColon4.Text = ":";
            // 
            // lbColon2
            // 
            this.lbColon2.AutoSize = true;
            this.lbColon2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbColon2.Location = new System.Drawing.Point(238, 51);
            this.lbColon2.Name = "lbColon2";
            this.lbColon2.Size = new System.Drawing.Size(17, 35);
            this.lbColon2.TabIndex = 5;
            this.lbColon2.Text = ":";
            // 
            // lbColon3
            // 
            this.lbColon3.AutoSize = true;
            this.lbColon3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbColon3.Location = new System.Drawing.Point(238, 102);
            this.lbColon3.Name = "lbColon3";
            this.lbColon3.Size = new System.Drawing.Size(17, 35);
            this.lbColon3.TabIndex = 6;
            this.lbColon3.Text = ":";
            // 
            // lbAngkaPendapatan
            // 
            this.lbAngkaPendapatan.AutoSize = true;
            this.lbAngkaPendapatan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbAngkaPendapatan.Location = new System.Drawing.Point(261, 0);
            this.lbAngkaPendapatan.Name = "lbAngkaPendapatan";
            this.lbAngkaPendapatan.Size = new System.Drawing.Size(61, 35);
            this.lbAngkaPendapatan.TabIndex = 7;
            this.lbAngkaPendapatan.Text = "Rp -";
            // 
            // lbAngkaJumlahTransaksi
            // 
            this.lbAngkaJumlahTransaksi.AutoSize = true;
            this.lbAngkaJumlahTransaksi.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbAngkaJumlahTransaksi.Location = new System.Drawing.Point(261, 51);
            this.lbAngkaJumlahTransaksi.Name = "lbAngkaJumlahTransaksi";
            this.lbAngkaJumlahTransaksi.Size = new System.Drawing.Size(24, 35);
            this.lbAngkaJumlahTransaksi.TabIndex = 8;
            this.lbAngkaJumlahTransaksi.Text = "-";
            // 
            // lbAngkaMakananTerjual
            // 
            this.lbAngkaMakananTerjual.AutoSize = true;
            this.lbAngkaMakananTerjual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbAngkaMakananTerjual.Location = new System.Drawing.Point(261, 102);
            this.lbAngkaMakananTerjual.Name = "lbAngkaMakananTerjual";
            this.lbAngkaMakananTerjual.Size = new System.Drawing.Size(24, 35);
            this.lbAngkaMakananTerjual.TabIndex = 9;
            this.lbAngkaMakananTerjual.Text = "-";
            // 
            // lbAngkaMinumanTerjual
            // 
            this.lbAngkaMinumanTerjual.AutoSize = true;
            this.lbAngkaMinumanTerjual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbAngkaMinumanTerjual.Location = new System.Drawing.Point(261, 150);
            this.lbAngkaMinumanTerjual.Name = "lbAngkaMinumanTerjual";
            this.lbAngkaMinumanTerjual.Size = new System.Drawing.Size(24, 35);
            this.lbAngkaMinumanTerjual.TabIndex = 10;
            this.lbAngkaMinumanTerjual.Text = "-";
            // 
            // gbMenuMakanan
            // 
            this.gbMenuMakanan.Controls.Add(this.flpDaftarMenuMakanan);
            this.gbMenuMakanan.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbMenuMakanan.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMenuMakanan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.gbMenuMakanan.Location = new System.Drawing.Point(3, 291);
            this.gbMenuMakanan.Margin = new System.Windows.Forms.Padding(3, 30, 3, 3);
            this.gbMenuMakanan.Name = "gbMenuMakanan";
            this.gbMenuMakanan.Padding = new System.Windows.Forms.Padding(10);
            this.gbMenuMakanan.Size = new System.Drawing.Size(1240, 370);
            this.gbMenuMakanan.TabIndex = 2;
            this.gbMenuMakanan.TabStop = false;
            this.gbMenuMakanan.Text = "Daftar Menu Makanan";
            // 
            // flpDaftarMenuMakanan
            // 
            this.flpDaftarMenuMakanan.AutoScroll = true;
            this.flpDaftarMenuMakanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpDaftarMenuMakanan.Location = new System.Drawing.Point(10, 45);
            this.flpDaftarMenuMakanan.Name = "flpDaftarMenuMakanan";
            this.flpDaftarMenuMakanan.Size = new System.Drawing.Size(1220, 315);
            this.flpDaftarMenuMakanan.TabIndex = 3;
            this.flpDaftarMenuMakanan.WrapContents = false;
            // 
            // gbMenuMinuman
            // 
            this.gbMenuMinuman.Controls.Add(this.flpDaftarMenuMinuman);
            this.gbMenuMinuman.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbMenuMinuman.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMenuMinuman.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.gbMenuMinuman.Location = new System.Drawing.Point(3, 694);
            this.gbMenuMinuman.Margin = new System.Windows.Forms.Padding(3, 30, 3, 3);
            this.gbMenuMinuman.Name = "gbMenuMinuman";
            this.gbMenuMinuman.Size = new System.Drawing.Size(1240, 370);
            this.gbMenuMinuman.TabIndex = 4;
            this.gbMenuMinuman.TabStop = false;
            this.gbMenuMinuman.Text = "Daftar Menu Minuman";
            // 
            // flpDaftarMenuMinuman
            // 
            this.flpDaftarMenuMinuman.AutoScroll = true;
            this.flpDaftarMenuMinuman.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpDaftarMenuMinuman.Location = new System.Drawing.Point(3, 38);
            this.flpDaftarMenuMinuman.Name = "flpDaftarMenuMinuman";
            this.flpDaftarMenuMinuman.Size = new System.Drawing.Size(1234, 329);
            this.flpDaftarMenuMinuman.TabIndex = 5;
            this.flpDaftarMenuMinuman.WrapContents = false;
            // 
            // gbDaftarStaff
            // 
            this.gbDaftarStaff.Controls.Add(this.flpDaftarStaff);
            this.gbDaftarStaff.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbDaftarStaff.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDaftarStaff.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.gbDaftarStaff.Location = new System.Drawing.Point(3, 1097);
            this.gbDaftarStaff.Margin = new System.Windows.Forms.Padding(3, 30, 3, 3);
            this.gbDaftarStaff.Name = "gbDaftarStaff";
            this.gbDaftarStaff.Size = new System.Drawing.Size(1240, 370);
            this.gbDaftarStaff.TabIndex = 6;
            this.gbDaftarStaff.TabStop = false;
            this.gbDaftarStaff.Text = "Daftar Staff";
            // 
            // flpDaftarStaff
            // 
            this.flpDaftarStaff.AutoScroll = true;
            this.flpDaftarStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpDaftarStaff.Location = new System.Drawing.Point(3, 38);
            this.flpDaftarStaff.Name = "flpDaftarStaff";
            this.flpDaftarStaff.Size = new System.Drawing.Size(1234, 329);
            this.flpDaftarStaff.TabIndex = 7;
            this.flpDaftarStaff.WrapContents = false;
            // 
            // lbKeterangan
            // 
            this.lbKeterangan.Cursor = System.Windows.Forms.Cursors.Default;
            this.lbKeterangan.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbKeterangan.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKeterangan.Location = new System.Drawing.Point(0, 68);
            this.lbKeterangan.Name = "lbKeterangan";
            this.lbKeterangan.Padding = new System.Windows.Forms.Padding(0, 0, 0, 50);
            this.lbKeterangan.Size = new System.Drawing.Size(1306, 118);
            this.lbKeterangan.TabIndex = 9;
            this.lbKeterangan.Text = "Anda login sebagai Admin. Anda bisa melihat statistik penjualan, menambah menu ma" +
    "kanan atau minuman,\r\n dan mengelola staff.";
            // 
            // lbSelamatDatang
            // 
            this.lbSelamatDatang.AutoSize = true;
            this.lbSelamatDatang.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbSelamatDatang.Font = new System.Drawing.Font("Calibri", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSelamatDatang.Location = new System.Drawing.Point(0, 0);
            this.lbSelamatDatang.Name = "lbSelamatDatang";
            this.lbSelamatDatang.Size = new System.Drawing.Size(400, 68);
            this.lbSelamatDatang.TabIndex = 8;
            this.lbSelamatDatang.Text = "Selamat Datang.";
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "food_icon.png");
            this.imageList.Images.SetKeyName(1, "logo_user.png");
            // 
            // Admin_Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1356, 747);
            this.Controls.Add(this.pnContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Admin_Home";
            this.Padding = new System.Windows.Forms.Padding(30, 30, 20, 50);
            this.ShowInTaskbar = false;
            this.Text = "LihatData";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Admin_Home_Load);
            this.pnContainer.ResumeLayout(false);
            this.pnContainer.PerformLayout();
            this.flpInfoContainer.ResumeLayout(false);
            this.gbStatistikPenjualan.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.gbMenuMakanan.ResumeLayout(false);
            this.gbMenuMinuman.ResumeLayout(false);
            this.gbDaftarStaff.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnContainer;
        private System.Windows.Forms.Label lbKeterangan;
        private System.Windows.Forms.Label lbSelamatDatang;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.FlowLayoutPanel flpInfoContainer;
        private System.Windows.Forms.GroupBox gbStatistikPenjualan;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lbMinumanTerjual;
        private System.Windows.Forms.Label lbColon1;
        private System.Windows.Forms.Label lbPendapatan;
        private System.Windows.Forms.Label lbJumlahTransaksi;
        private System.Windows.Forms.Label lbMakananTerjual;
        private System.Windows.Forms.Label lbColon4;
        private System.Windows.Forms.Label lbColon2;
        private System.Windows.Forms.Label lbColon3;
        private System.Windows.Forms.Label lbAngkaPendapatan;
        private System.Windows.Forms.Label lbAngkaJumlahTransaksi;
        private System.Windows.Forms.Label lbAngkaMakananTerjual;
        private System.Windows.Forms.Label lbAngkaMinumanTerjual;
        private System.Windows.Forms.GroupBox gbMenuMakanan;
        private System.Windows.Forms.FlowLayoutPanel flpDaftarMenuMakanan;
        private System.Windows.Forms.GroupBox gbMenuMinuman;
        private System.Windows.Forms.FlowLayoutPanel flpDaftarMenuMinuman;
        private System.Windows.Forms.GroupBox gbDaftarStaff;
        private System.Windows.Forms.FlowLayoutPanel flpDaftarStaff;
    }
}