﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Chickenfork_POS
{
    public partial class Admin_Home : Form
    {
        public Admin_Home()
        {
            InitializeComponent();
        }

        private void Admin_Home_Load(object sender, EventArgs e)
        {
            lbAngkaPendapatan.Text = "Rp " + Connection.getStatistikPenjualan("hitung_total_pendapatan");
            lbAngkaJumlahTransaksi.Text = Connection.getStatistikPenjualan("jumlah_transaksi");
            lbAngkaMakananTerjual.Text = Connection.getStatistikPenjualan("makanan_terjual");
            lbAngkaMinumanTerjual.Text = Connection.getStatistikPenjualan("minuman_terjual");

            addToFlowLayoutPanel("daftar_makanan", flpDaftarMenuMakanan, "makanan");
            addToFlowLayoutPanel("daftar_minuman", flpDaftarMenuMinuman, "minuman");
            addToFlowLayoutPanel("daftar_staff", flpDaftarStaff, "staff");
        }

        private void addToFlowLayoutPanel(string nama_procedure, FlowLayoutPanel flp, string jenis)
        {
            List<MenuItem> itemList = Connection.getMenuItem(nama_procedure, this.imageList, jenis);
            foreach (MenuItem item in itemList)
            {
                flp.Controls.Add(item);
            }
        }
    }
}
