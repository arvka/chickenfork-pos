﻿namespace Chickenfork_POS
{
    partial class Admin_KelolaAkun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbTTL = new System.Windows.Forms.Label();
            this.btUploadFotoStaff = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbNamaStaff = new System.Windows.Forms.Label();
            this.tbTTL = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.pbFotoStaff = new System.Windows.Forms.PictureBox();
            this.tbNamaStaff = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.pnCenter = new System.Windows.Forms.Panel();
            this.btSimpan = new MaterialSkin.Controls.MaterialRaisedButton();
            this.cbJenisStaff = new MetroFramework.Controls.MetroComboBox();
            this.lbJenisStaff = new System.Windows.Forms.Label();
            this.lbPassword = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.tbPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tbUsername = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.pnWrapper = new System.Windows.Forms.Panel();
            this.openFotoStaff = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoStaff)).BeginInit();
            this.pnCenter.SuspendLayout();
            this.pnWrapper.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbTTL
            // 
            this.lbTTL.AutoSize = true;
            this.lbTTL.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTTL.Location = new System.Drawing.Point(33, 383);
            this.lbTTL.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTTL.Name = "lbTTL";
            this.lbTTL.Size = new System.Drawing.Size(175, 23);
            this.lbTTL.TabIndex = 6;
            this.lbTTL.Text = "Tempat, Tanggal Lahir";
            // 
            // btUploadFotoStaff
            // 
            this.btUploadFotoStaff.AutoSize = true;
            this.btUploadFotoStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btUploadFotoStaff.Depth = 0;
            this.btUploadFotoStaff.Icon = null;
            this.btUploadFotoStaff.Location = new System.Drawing.Point(321, 226);
            this.btUploadFotoStaff.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btUploadFotoStaff.MinimumSize = new System.Drawing.Size(228, 32);
            this.btUploadFotoStaff.MouseState = MaterialSkin.MouseState.HOVER;
            this.btUploadFotoStaff.Name = "btUploadFotoStaff";
            this.btUploadFotoStaff.Primary = true;
            this.btUploadFotoStaff.Size = new System.Drawing.Size(228, 36);
            this.btUploadFotoStaff.TabIndex = 1;
            this.btUploadFotoStaff.Text = "Upload Foto Staff";
            this.btUploadFotoStaff.UseVisualStyleBackColor = true;
            this.btUploadFotoStaff.Click += new System.EventHandler(this.btUploadFotoStaff_Click);
            // 
            // lbNamaStaff
            // 
            this.lbNamaStaff.AutoSize = true;
            this.lbNamaStaff.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNamaStaff.Location = new System.Drawing.Point(33, 314);
            this.lbNamaStaff.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbNamaStaff.Name = "lbNamaStaff";
            this.lbNamaStaff.Size = new System.Drawing.Size(94, 23);
            this.lbNamaStaff.TabIndex = 4;
            this.lbNamaStaff.Text = "Nama Staff";
            // 
            // tbTTL
            // 
            this.tbTTL.Depth = 0;
            this.tbTTL.Hint = "Tempat, DD/MM/YYYY";
            this.tbTTL.Location = new System.Drawing.Point(37, 417);
            this.tbTTL.MaxLength = 32767;
            this.tbTTL.MinimumSize = new System.Drawing.Size(467, 21);
            this.tbTTL.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbTTL.Name = "tbTTL";
            this.tbTTL.PasswordChar = '\0';
            this.tbTTL.SelectedText = "";
            this.tbTTL.SelectionLength = 0;
            this.tbTTL.SelectionStart = 0;
            this.tbTTL.Size = new System.Drawing.Size(530, 23);
            this.tbTTL.TabIndex = 3;
            this.tbTTL.TabStop = false;
            this.tbTTL.UseSystemPasswordChar = false;
            // 
            // pbFotoStaff
            // 
            this.pbFotoStaff.Image = global::Chickenfork_POS.Properties.Resources.logo_user;
            this.pbFotoStaff.Location = new System.Drawing.Point(321, 12);
            this.pbFotoStaff.Name = "pbFotoStaff";
            this.pbFotoStaff.Size = new System.Drawing.Size(228, 215);
            this.pbFotoStaff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFotoStaff.TabIndex = 2;
            this.pbFotoStaff.TabStop = false;
            // 
            // tbNamaStaff
            // 
            this.tbNamaStaff.Depth = 0;
            this.tbNamaStaff.Hint = "Masukkan nama staff";
            this.tbNamaStaff.Location = new System.Drawing.Point(37, 348);
            this.tbNamaStaff.MaxLength = 32767;
            this.tbNamaStaff.MinimumSize = new System.Drawing.Size(467, 21);
            this.tbNamaStaff.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbNamaStaff.Name = "tbNamaStaff";
            this.tbNamaStaff.PasswordChar = '\0';
            this.tbNamaStaff.SelectedText = "";
            this.tbNamaStaff.SelectionLength = 0;
            this.tbNamaStaff.SelectionStart = 0;
            this.tbNamaStaff.Size = new System.Drawing.Size(530, 23);
            this.tbNamaStaff.TabIndex = 2;
            this.tbNamaStaff.TabStop = false;
            this.tbNamaStaff.UseSystemPasswordChar = false;
            // 
            // pnCenter
            // 
            this.pnCenter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnCenter.BackColor = System.Drawing.Color.White;
            this.pnCenter.Controls.Add(this.btSimpan);
            this.pnCenter.Controls.Add(this.cbJenisStaff);
            this.pnCenter.Controls.Add(this.lbJenisStaff);
            this.pnCenter.Controls.Add(this.lbPassword);
            this.pnCenter.Controls.Add(this.lbUsername);
            this.pnCenter.Controls.Add(this.tbPassword);
            this.pnCenter.Controls.Add(this.tbUsername);
            this.pnCenter.Controls.Add(this.lbTTL);
            this.pnCenter.Controls.Add(this.btUploadFotoStaff);
            this.pnCenter.Controls.Add(this.lbNamaStaff);
            this.pnCenter.Controls.Add(this.tbTTL);
            this.pnCenter.Controls.Add(this.pbFotoStaff);
            this.pnCenter.Controls.Add(this.tbNamaStaff);
            this.pnCenter.Location = new System.Drawing.Point(-2, -5);
            this.pnCenter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnCenter.Name = "pnCenter";
            this.pnCenter.Size = new System.Drawing.Size(871, 517);
            this.pnCenter.TabIndex = 2;
            // 
            // btSimpan
            // 
            this.btSimpan.AutoSize = true;
            this.btSimpan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSimpan.Depth = 0;
            this.btSimpan.Icon = null;
            this.btSimpan.Location = new System.Drawing.Point(441, 458);
            this.btSimpan.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btSimpan.MinimumSize = new System.Drawing.Size(353, 32);
            this.btSimpan.MouseState = MaterialSkin.MouseState.HOVER;
            this.btSimpan.Name = "btSimpan";
            this.btSimpan.Primary = true;
            this.btSimpan.Size = new System.Drawing.Size(353, 36);
            this.btSimpan.TabIndex = 7;
            this.btSimpan.Text = "Simpan";
            this.btSimpan.UseVisualStyleBackColor = true;
            this.btSimpan.Click += new System.EventHandler(this.btSimpan_Click);
            // 
            // cbJenisStaff
            // 
            this.cbJenisStaff.FontSize = MetroFramework.MetroComboBoxSize.Tall;
            this.cbJenisStaff.FormattingEnabled = true;
            this.cbJenisStaff.ItemHeight = 29;
            this.cbJenisStaff.Items.AddRange(new object[] {
            "admin",
            "kasir"});
            this.cbJenisStaff.Location = new System.Drawing.Point(126, 454);
            this.cbJenisStaff.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbJenisStaff.Name = "cbJenisStaff";
            this.cbJenisStaff.Size = new System.Drawing.Size(265, 35);
            this.cbJenisStaff.TabIndex = 6;
            this.cbJenisStaff.UseSelectable = true;
            // 
            // lbJenisStaff
            // 
            this.lbJenisStaff.AutoSize = true;
            this.lbJenisStaff.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJenisStaff.Location = new System.Drawing.Point(33, 461);
            this.lbJenisStaff.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbJenisStaff.Name = "lbJenisStaff";
            this.lbJenisStaff.Size = new System.Drawing.Size(81, 23);
            this.lbJenisStaff.TabIndex = 12;
            this.lbJenisStaff.Text = "Jenis Staf";
            // 
            // lbPassword
            // 
            this.lbPassword.AutoSize = true;
            this.lbPassword.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPassword.Location = new System.Drawing.Point(437, 383);
            this.lbPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(86, 23);
            this.lbPassword.TabIndex = 10;
            this.lbPassword.Text = "Password";
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUsername.Location = new System.Drawing.Point(438, 314);
            this.lbUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(89, 23);
            this.lbUsername.TabIndex = 9;
            this.lbUsername.Text = "Username";
            // 
            // tbPassword
            // 
            this.tbPassword.Depth = 0;
            this.tbPassword.Hint = "Masukkan password";
            this.tbPassword.Location = new System.Drawing.Point(441, 417);
            this.tbPassword.MaxLength = 32767;
            this.tbPassword.MinimumSize = new System.Drawing.Size(530, 21);
            this.tbPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.SelectedText = "";
            this.tbPassword.SelectionLength = 0;
            this.tbPassword.SelectionStart = 0;
            this.tbPassword.Size = new System.Drawing.Size(530, 23);
            this.tbPassword.TabIndex = 5;
            this.tbPassword.TabStop = false;
            this.tbPassword.UseSystemPasswordChar = true;
            // 
            // tbUsername
            // 
            this.tbUsername.Depth = 0;
            this.tbUsername.Hint = "Masukkan username";
            this.tbUsername.Location = new System.Drawing.Point(442, 348);
            this.tbUsername.MaxLength = 32767;
            this.tbUsername.MinimumSize = new System.Drawing.Size(530, 21);
            this.tbUsername.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.PasswordChar = '\0';
            this.tbUsername.SelectedText = "";
            this.tbUsername.SelectionLength = 0;
            this.tbUsername.SelectionStart = 0;
            this.tbUsername.Size = new System.Drawing.Size(530, 23);
            this.tbUsername.TabIndex = 4;
            this.tbUsername.TabStop = false;
            this.tbUsername.UseSystemPasswordChar = false;
            // 
            // pnWrapper
            // 
            this.pnWrapper.AutoScroll = true;
            this.pnWrapper.BackColor = System.Drawing.Color.White;
            this.pnWrapper.Controls.Add(this.pnCenter);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 0);
            this.pnWrapper.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.Padding = new System.Windows.Forms.Padding(13, 13, 13, 13);
            this.pnWrapper.Size = new System.Drawing.Size(867, 507);
            this.pnWrapper.TabIndex = 8;
            // 
            // openFotoStaff
            // 
            this.openFotoStaff.Filter = "File Gambar | *.png;*.jpg;*.jpeg";
            this.openFotoStaff.Title = "Pilih Foto Staff";
            // 
            // Admin_KelolaAkun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 507);
            this.Controls.Add(this.pnWrapper);
            this.Name = "Admin_KelolaAkun";
            this.ShowInTaskbar = false;
            this.Text = "TambahAkun";
            this.Load += new System.EventHandler(this.Admin_KelolaAkun_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoStaff)).EndInit();
            this.pnCenter.ResumeLayout(false);
            this.pnCenter.PerformLayout();
            this.pnWrapper.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lbTTL;
        private MaterialSkin.Controls.MaterialRaisedButton btUploadFotoStaff;
        private System.Windows.Forms.Label lbNamaStaff;
        private MaterialSkin.Controls.MaterialSingleLineTextField tbTTL;
        private System.Windows.Forms.PictureBox pbFotoStaff;
        private MaterialSkin.Controls.MaterialSingleLineTextField tbNamaStaff;
        private System.Windows.Forms.Panel pnCenter;
        private System.Windows.Forms.Panel pnWrapper;
        private System.Windows.Forms.Label lbPassword;
        private System.Windows.Forms.Label lbUsername;
        private MaterialSkin.Controls.MaterialSingleLineTextField tbPassword;
        private MaterialSkin.Controls.MaterialSingleLineTextField tbUsername;
        private System.Windows.Forms.Label lbJenisStaff;
        private MetroFramework.Controls.MetroComboBox cbJenisStaff;
        private MaterialSkin.Controls.MaterialRaisedButton btSimpan;
        private System.Windows.Forms.OpenFileDialog openFotoStaff;
    }
}