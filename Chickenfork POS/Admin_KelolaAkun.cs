﻿using System;
using System.IO;
using MaterialSkin;
using System.Drawing;
using System.Windows.Forms;
using MaterialSkin.Controls;

namespace Chickenfork_POS
{
    public partial class Admin_KelolaAkun : MaterialForm
    {
        #region Deklarasi variabel
        private string filePath;
        #endregion

        #region Constructor
        public Admin_KelolaAkun()
        {
            InitializeComponent();
        }
        #endregion

        #region Event handler method
        private void Admin_KelolaAkun_Load(object sender, EventArgs e)
        {
            //MaterialSkin form setting
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Red600, Primary.Red600, Primary.Red600, Accent.Red200, TextShade.WHITE);

            //Inisialisasi variabel
            this.filePath = "";
            this.cbJenisStaff.SelectedIndex = 1;
        }

        private void btUploadFotoStaff_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dlgResult = this.openFotoStaff.ShowDialog();
                if (dlgResult == DialogResult.OK)
                {
                    this.filePath = openFotoStaff.FileName;

                    this.pbFotoStaff.Image = Image.FromFile(filePath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan pada Upload Foto");
            }
        }

        private void btSimpan_Click(object sender, EventArgs e)
        {
            try
            {
                string username_staf = tbUsername.Text;
                string jenis = cbJenisStaff.Text;
                string ttl = tbTTL.Text;
                string password = tbPassword.Text;

                //Cek semua field diisi
                if (username_staf == "" || cbJenisStaff.SelectedIndex == -1 || filePath == "")
                {
                    MessageBox.Show("Semua field dan foto harus diisi.", "Peringatan");
                }
                else
                {
                    //Copy File
                    string resourcePath = Path.GetDirectoryName(Application.ExecutablePath) + @"\res\";
                    string namaFile = Path.GetFileName(filePath);
                    File.Copy(filePath, resourcePath + namaFile, true);

                    //Insert ke database
                    string param = "'" + username_staf + 
                        "', '" + jenis + 
                        "', '" + namaFile + 
                        "', '" + password + 
                        "', '" + ttl + "'";
                    Connection.tambahItem("tambah_staf", param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan pada Insert Akun");
            }

            //reset field
            this.pbFotoStaff.Image = Chickenfork_POS.Properties.Resources.food_icon;
            this.tbNamaStaff.Text = "";
            this.tbTTL.Text = "";
            this.tbUsername.Text = "";
            this.tbPassword.Text = "";
            this.cbJenisStaff.SelectedIndex = 0;
            this.filePath = "";
        }
        #endregion
    }
}
