﻿namespace Chickenfork_POS
{
    partial class Admin_TambahMenuMakanan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbHargaMakanan = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.tbNamaMakanan = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.pnCenter = new System.Windows.Forms.Panel();
            this.btSimpan = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbHargaMakanan = new System.Windows.Forms.Label();
            this.btUploadFotoMakanan = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbNamaMakanan = new System.Windows.Forms.Label();
            this.pbFotoMakanan = new System.Windows.Forms.PictureBox();
            this.pnContainer = new System.Windows.Forms.Panel();
            this.openFotoMakanan = new System.Windows.Forms.OpenFileDialog();
            this.pnCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoMakanan)).BeginInit();
            this.pnContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbHargaMakanan
            // 
            this.tbHargaMakanan.Depth = 0;
            this.tbHargaMakanan.Hint = "Masukkan harga makanan";
            this.tbHargaMakanan.Location = new System.Drawing.Point(55, 608);
            this.tbHargaMakanan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHargaMakanan.MaxLength = 32767;
            this.tbHargaMakanan.MinimumSize = new System.Drawing.Size(795, 32);
            this.tbHargaMakanan.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbHargaMakanan.Name = "tbHargaMakanan";
            this.tbHargaMakanan.PasswordChar = '\0';
            this.tbHargaMakanan.SelectedText = "";
            this.tbHargaMakanan.SelectionLength = 0;
            this.tbHargaMakanan.SelectionStart = 0;
            this.tbHargaMakanan.Size = new System.Drawing.Size(795, 32);
            this.tbHargaMakanan.TabIndex = 3;
            this.tbHargaMakanan.TabStop = false;
            this.tbHargaMakanan.UseSystemPasswordChar = false;
            // 
            // tbNamaMakanan
            // 
            this.tbNamaMakanan.Depth = 0;
            this.tbNamaMakanan.Hint = "Masukkan nama makanan";
            this.tbNamaMakanan.Location = new System.Drawing.Point(55, 504);
            this.tbNamaMakanan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbNamaMakanan.MaxLength = 32767;
            this.tbNamaMakanan.MinimumSize = new System.Drawing.Size(795, 32);
            this.tbNamaMakanan.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbNamaMakanan.Name = "tbNamaMakanan";
            this.tbNamaMakanan.PasswordChar = '\0';
            this.tbNamaMakanan.SelectedText = "";
            this.tbNamaMakanan.SelectionLength = 0;
            this.tbNamaMakanan.SelectionStart = 0;
            this.tbNamaMakanan.Size = new System.Drawing.Size(795, 32);
            this.tbNamaMakanan.TabIndex = 2;
            this.tbNamaMakanan.TabStop = false;
            this.tbNamaMakanan.UseSystemPasswordChar = false;
            // 
            // pnCenter
            // 
            this.pnCenter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnCenter.Controls.Add(this.btSimpan);
            this.pnCenter.Controls.Add(this.lbHargaMakanan);
            this.pnCenter.Controls.Add(this.btUploadFotoMakanan);
            this.pnCenter.Controls.Add(this.lbNamaMakanan);
            this.pnCenter.Controls.Add(this.tbHargaMakanan);
            this.pnCenter.Controls.Add(this.pbFotoMakanan);
            this.pnCenter.Controls.Add(this.tbNamaMakanan);
            this.pnCenter.Location = new System.Drawing.Point(328, 52);
            this.pnCenter.Name = "pnCenter";
            this.pnCenter.Size = new System.Drawing.Size(679, 737);
            this.pnCenter.TabIndex = 2;
            // 
            // btSimpan
            // 
            this.btSimpan.AutoSize = true;
            this.btSimpan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSimpan.Depth = 0;
            this.btSimpan.Icon = null;
            this.btSimpan.Location = new System.Drawing.Point(55, 660);
            this.btSimpan.MinimumSize = new System.Drawing.Size(530, 50);
            this.btSimpan.MouseState = MaterialSkin.MouseState.HOVER;
            this.btSimpan.Name = "btSimpan";
            this.btSimpan.Primary = true;
            this.btSimpan.Size = new System.Drawing.Size(530, 50);
            this.btSimpan.TabIndex = 4;
            this.btSimpan.Text = "Simpan";
            this.btSimpan.UseVisualStyleBackColor = true;
            this.btSimpan.Click += new System.EventHandler(this.btSimpan_Click);
            // 
            // lbHargaMakanan
            // 
            this.lbHargaMakanan.AutoSize = true;
            this.lbHargaMakanan.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHargaMakanan.Location = new System.Drawing.Point(49, 555);
            this.lbHargaMakanan.Name = "lbHargaMakanan";
            this.lbHargaMakanan.Size = new System.Drawing.Size(192, 35);
            this.lbHargaMakanan.TabIndex = 6;
            this.lbHargaMakanan.Text = "Harga Makanan";
            // 
            // btUploadFotoMakanan
            // 
            this.btUploadFotoMakanan.AutoSize = true;
            this.btUploadFotoMakanan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btUploadFotoMakanan.Depth = 0;
            this.btUploadFotoMakanan.Icon = null;
            this.btUploadFotoMakanan.Location = new System.Drawing.Point(168, 351);
            this.btUploadFotoMakanan.MinimumSize = new System.Drawing.Size(342, 50);
            this.btUploadFotoMakanan.MouseState = MaterialSkin.MouseState.HOVER;
            this.btUploadFotoMakanan.Name = "btUploadFotoMakanan";
            this.btUploadFotoMakanan.Primary = true;
            this.btUploadFotoMakanan.Size = new System.Drawing.Size(342, 50);
            this.btUploadFotoMakanan.TabIndex = 1;
            this.btUploadFotoMakanan.Text = "Upload Foto Makanan";
            this.btUploadFotoMakanan.UseVisualStyleBackColor = true;
            this.btUploadFotoMakanan.Click += new System.EventHandler(this.btUploadFotoMakanan_Click);
            // 
            // lbNamaMakanan
            // 
            this.lbNamaMakanan.AutoSize = true;
            this.lbNamaMakanan.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNamaMakanan.Location = new System.Drawing.Point(49, 451);
            this.lbNamaMakanan.Name = "lbNamaMakanan";
            this.lbNamaMakanan.Size = new System.Drawing.Size(193, 35);
            this.lbNamaMakanan.TabIndex = 4;
            this.lbNamaMakanan.Text = "Nama Makanan";
            // 
            // pbFotoMakanan
            // 
            this.pbFotoMakanan.Image = global::Chickenfork_POS.Properties.Resources.food_icon;
            this.pbFotoMakanan.InitialImage = null;
            this.pbFotoMakanan.Location = new System.Drawing.Point(168, 19);
            this.pbFotoMakanan.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbFotoMakanan.Name = "pbFotoMakanan";
            this.pbFotoMakanan.Size = new System.Drawing.Size(342, 331);
            this.pbFotoMakanan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFotoMakanan.TabIndex = 2;
            this.pbFotoMakanan.TabStop = false;
            // 
            // pnContainer
            // 
            this.pnContainer.Controls.Add(this.pnCenter);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.Padding = new System.Windows.Forms.Padding(20);
            this.pnContainer.Size = new System.Drawing.Size(1335, 840);
            this.pnContainer.TabIndex = 6;
            // 
            // openFotoMakanan
            // 
            this.openFotoMakanan.Filter = "File Gambar | *.png;*.jpg;*.jpeg";
            this.openFotoMakanan.Title = "Pilih Foto Makanan";
            // 
            // Admin_TambahMenuMakanan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1335, 840);
            this.Controls.Add(this.pnContainer);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Admin_TambahMenuMakanan";
            this.ShowInTaskbar = false;
            this.Text = "Tambah_Makanan";
            this.Load += new System.EventHandler(this.Admin_TambahMenuMakanan_Load);
            this.pnCenter.ResumeLayout(false);
            this.pnCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoMakanan)).EndInit();
            this.pnContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MaterialSkin.Controls.MaterialSingleLineTextField tbHargaMakanan;
        private MaterialSkin.Controls.MaterialSingleLineTextField tbNamaMakanan;
        private System.Windows.Forms.Panel pnCenter;
        private System.Windows.Forms.PictureBox pbFotoMakanan;
        private MaterialSkin.Controls.MaterialRaisedButton btUploadFotoMakanan;
        private System.Windows.Forms.Label lbNamaMakanan;
        private System.Windows.Forms.Panel pnContainer;
        private System.Windows.Forms.Label lbHargaMakanan;
        private System.Windows.Forms.OpenFileDialog openFotoMakanan;
        private MaterialSkin.Controls.MaterialRaisedButton btSimpan;
    }
}