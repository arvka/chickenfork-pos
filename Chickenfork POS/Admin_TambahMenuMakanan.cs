﻿using System;
using System.IO;
using MaterialSkin;
using System.Drawing;
using System.Windows.Forms;
using MaterialSkin.Controls;

namespace Chickenfork_POS
{
    public partial class Admin_TambahMenuMakanan : MaterialForm
    {
        #region Deklarasi variabel
        private string filePath;
        #endregion

        #region Constructor
        public Admin_TambahMenuMakanan()
        {
            InitializeComponent();
        }
        #endregion

        #region Event handler method
        private void Admin_TambahMenuMakanan_Load(object sender, EventArgs e)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Red600, Primary.Red600, Primary.Red600, Accent.Red200, TextShade.WHITE);

            this.filePath = "";
        }

        private void btUploadFotoMakanan_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dlgResult = this.openFotoMakanan.ShowDialog();
                if (dlgResult == DialogResult.OK)
                {
                    filePath = openFotoMakanan.FileName;

                    pbFotoMakanan.Image = Image.FromFile(filePath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan pada Upload Foto");
            }
        }

        private void btSimpan_Click(object sender, EventArgs e)
        {
            try
            {
                string nama_makanan = tbNamaMakanan.Text;
                int harga_makanan = Int32.Parse(tbHargaMakanan.Text);

                if (nama_makanan == "" || filePath == "")
                {
                    MessageBox.Show("Semua field dan foto harus diisi.", "Peringatan");
                }
                else
                {
                    //Copy File
                    string directoryPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\res\";
                    string namaFile = Path.GetFileName(filePath);
                    File.Copy(filePath, directoryPath + namaFile, true);

                    //Insert ke database
                    string param = "'" + nama_makanan + "', '" + harga_makanan.ToString() + "', '" + namaFile + "'";
                    Connection.tambahItem("tambah_makanan", param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan pada Tambah Menu Makanan");
            }

            //reset field
            this.pbFotoMakanan.Image = Chickenfork_POS.Properties.Resources.food_icon;
            this.tbNamaMakanan.Text = "";
            this.tbHargaMakanan.Text = "";
            this.filePath = "";
        }
        #endregion
    }
}

