﻿namespace Chickenfork_POS
{
    partial class Admin_TambahMenuMinuman
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnContainer = new System.Windows.Forms.Panel();
            this.pnCenter = new System.Windows.Forms.Panel();
            this.btSimpan = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbHargaMinuman = new System.Windows.Forms.Label();
            this.btUploadFotoMakanan = new MaterialSkin.Controls.MaterialRaisedButton();
            this.lbNamaMinuman = new System.Windows.Forms.Label();
            this.tbHargaMinuman = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.pbFotoMinuman = new System.Windows.Forms.PictureBox();
            this.tbNamaMinuman = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.openFotoMinuman = new System.Windows.Forms.OpenFileDialog();
            this.pnContainer.SuspendLayout();
            this.pnCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoMinuman)).BeginInit();
            this.SuspendLayout();
            // 
            // pnContainer
            // 
            this.pnContainer.Controls.Add(this.pnCenter);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.Padding = new System.Windows.Forms.Padding(20);
            this.pnContainer.Size = new System.Drawing.Size(1358, 865);
            this.pnContainer.TabIndex = 7;
            // 
            // pnCenter
            // 
            this.pnCenter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnCenter.Controls.Add(this.btSimpan);
            this.pnCenter.Controls.Add(this.lbHargaMinuman);
            this.pnCenter.Controls.Add(this.btUploadFotoMakanan);
            this.pnCenter.Controls.Add(this.lbNamaMinuman);
            this.pnCenter.Controls.Add(this.tbHargaMinuman);
            this.pnCenter.Controls.Add(this.pbFotoMinuman);
            this.pnCenter.Controls.Add(this.tbNamaMinuman);
            this.pnCenter.Location = new System.Drawing.Point(340, 64);
            this.pnCenter.Name = "pnCenter";
            this.pnCenter.Size = new System.Drawing.Size(679, 737);
            this.pnCenter.TabIndex = 3;
            // 
            // btSimpan
            // 
            this.btSimpan.AutoSize = true;
            this.btSimpan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSimpan.Depth = 0;
            this.btSimpan.Icon = null;
            this.btSimpan.Location = new System.Drawing.Point(55, 660);
            this.btSimpan.MinimumSize = new System.Drawing.Size(530, 50);
            this.btSimpan.MouseState = MaterialSkin.MouseState.HOVER;
            this.btSimpan.Name = "btSimpan";
            this.btSimpan.Primary = true;
            this.btSimpan.Size = new System.Drawing.Size(530, 50);
            this.btSimpan.TabIndex = 4;
            this.btSimpan.Text = "Simpan";
            this.btSimpan.UseVisualStyleBackColor = true;
            this.btSimpan.Click += new System.EventHandler(this.btSimpan_Click);
            // 
            // lbHargaMinuman
            // 
            this.lbHargaMinuman.AutoSize = true;
            this.lbHargaMinuman.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHargaMinuman.Location = new System.Drawing.Point(49, 555);
            this.lbHargaMinuman.Name = "lbHargaMinuman";
            this.lbHargaMinuman.Size = new System.Drawing.Size(196, 35);
            this.lbHargaMinuman.TabIndex = 6;
            this.lbHargaMinuman.Text = "Harga Minuman";
            // 
            // btUploadFotoMakanan
            // 
            this.btUploadFotoMakanan.AutoSize = true;
            this.btUploadFotoMakanan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btUploadFotoMakanan.Depth = 0;
            this.btUploadFotoMakanan.Icon = null;
            this.btUploadFotoMakanan.Location = new System.Drawing.Point(168, 351);
            this.btUploadFotoMakanan.MinimumSize = new System.Drawing.Size(342, 50);
            this.btUploadFotoMakanan.MouseState = MaterialSkin.MouseState.HOVER;
            this.btUploadFotoMakanan.Name = "btUploadFotoMakanan";
            this.btUploadFotoMakanan.Primary = true;
            this.btUploadFotoMakanan.Size = new System.Drawing.Size(342, 50);
            this.btUploadFotoMakanan.TabIndex = 1;
            this.btUploadFotoMakanan.Text = "Upload Foto Minuman";
            this.btUploadFotoMakanan.UseVisualStyleBackColor = true;
            this.btUploadFotoMakanan.Click += new System.EventHandler(this.btUploadFotoMinuman_Click);
            // 
            // lbNamaMinuman
            // 
            this.lbNamaMinuman.AutoSize = true;
            this.lbNamaMinuman.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNamaMinuman.Location = new System.Drawing.Point(49, 451);
            this.lbNamaMinuman.Name = "lbNamaMinuman";
            this.lbNamaMinuman.Size = new System.Drawing.Size(197, 35);
            this.lbNamaMinuman.TabIndex = 4;
            this.lbNamaMinuman.Text = "Nama Minuman";
            // 
            // tbHargaMinuman
            // 
            this.tbHargaMinuman.Depth = 0;
            this.tbHargaMinuman.Hint = "Masukkan harga minuman";
            this.tbHargaMinuman.Location = new System.Drawing.Point(55, 608);
            this.tbHargaMinuman.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbHargaMinuman.MaxLength = 32767;
            this.tbHargaMinuman.MinimumSize = new System.Drawing.Size(795, 32);
            this.tbHargaMinuman.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbHargaMinuman.Name = "tbHargaMinuman";
            this.tbHargaMinuman.PasswordChar = '\0';
            this.tbHargaMinuman.SelectedText = "";
            this.tbHargaMinuman.SelectionLength = 0;
            this.tbHargaMinuman.SelectionStart = 0;
            this.tbHargaMinuman.Size = new System.Drawing.Size(795, 32);
            this.tbHargaMinuman.TabIndex = 3;
            this.tbHargaMinuman.TabStop = false;
            this.tbHargaMinuman.UseSystemPasswordChar = false;
            // 
            // pbFotoMinuman
            // 
            this.pbFotoMinuman.Image = global::Chickenfork_POS.Properties.Resources.food_icon;
            this.pbFotoMinuman.InitialImage = null;
            this.pbFotoMinuman.Location = new System.Drawing.Point(168, 19);
            this.pbFotoMinuman.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pbFotoMinuman.Name = "pbFotoMinuman";
            this.pbFotoMinuman.Size = new System.Drawing.Size(342, 331);
            this.pbFotoMinuman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFotoMinuman.TabIndex = 2;
            this.pbFotoMinuman.TabStop = false;
            // 
            // tbNamaMinuman
            // 
            this.tbNamaMinuman.Depth = 0;
            this.tbNamaMinuman.Hint = "Masukkan nama minuman";
            this.tbNamaMinuman.Location = new System.Drawing.Point(55, 504);
            this.tbNamaMinuman.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbNamaMinuman.MaxLength = 32767;
            this.tbNamaMinuman.MinimumSize = new System.Drawing.Size(795, 32);
            this.tbNamaMinuman.MouseState = MaterialSkin.MouseState.HOVER;
            this.tbNamaMinuman.Name = "tbNamaMinuman";
            this.tbNamaMinuman.PasswordChar = '\0';
            this.tbNamaMinuman.SelectedText = "";
            this.tbNamaMinuman.SelectionLength = 0;
            this.tbNamaMinuman.SelectionStart = 0;
            this.tbNamaMinuman.Size = new System.Drawing.Size(795, 32);
            this.tbNamaMinuman.TabIndex = 2;
            this.tbNamaMinuman.TabStop = false;
            this.tbNamaMinuman.UseSystemPasswordChar = false;
            // 
            // openFotoMinuman
            // 
            this.openFotoMinuman.Filter = "File Gambar | *.png;*.jpg;*.jpeg";
            this.openFotoMinuman.Title = "Pilih Foto Minuman";
            // 
            // Admin_TambahMenuMinuman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1358, 865);
            this.Controls.Add(this.pnContainer);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Admin_TambahMenuMinuman";
            this.ShowInTaskbar = false;
            this.Text = "TambahMinuman";
            this.Load += new System.EventHandler(this.Admin_TambahMenuMinuman_Load);
            this.pnContainer.ResumeLayout(false);
            this.pnCenter.ResumeLayout(false);
            this.pnCenter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFotoMinuman)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnContainer;
        private System.Windows.Forms.Panel pnCenter;
        private MaterialSkin.Controls.MaterialRaisedButton btSimpan;
        private System.Windows.Forms.Label lbHargaMinuman;
        private MaterialSkin.Controls.MaterialRaisedButton btUploadFotoMakanan;
        private System.Windows.Forms.Label lbNamaMinuman;
        private MaterialSkin.Controls.MaterialSingleLineTextField tbHargaMinuman;
        private System.Windows.Forms.PictureBox pbFotoMinuman;
        private MaterialSkin.Controls.MaterialSingleLineTextField tbNamaMinuman;
        private System.Windows.Forms.OpenFileDialog openFotoMinuman;
    }
}