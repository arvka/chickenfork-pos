﻿using System;
using System.IO;
using MaterialSkin;
using System.Drawing;
using System.Windows.Forms;
using MaterialSkin.Controls;

namespace Chickenfork_POS
{
    public partial class Admin_TambahMenuMinuman : MaterialForm
    {
        #region Deklarasi variabel
        private string filePath;
        #endregion

        #region Constructor
        public Admin_TambahMenuMinuman()
        {
            InitializeComponent();
        }
        #endregion

        #region Event handler method
        private void Admin_TambahMenuMinuman_Load(object sender, EventArgs e)
        {
            //MaterialSkin form setting
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Red600, Primary.Red600, Primary.Red600, Accent.Red200, TextShade.WHITE);

            //Inisialisasi variabel
            this.filePath = "";
        }

        private void btUploadFotoMinuman_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dlgResult = this.openFotoMinuman.ShowDialog();
                if (dlgResult == DialogResult.OK)
                {
                    filePath = openFotoMinuman.FileName;

                    pbFotoMinuman.Image = Image.FromFile(filePath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan pada Upload Foto");
            }
        }

        private void btSimpan_Click(object sender, EventArgs e)
        {
            try
            {
                string nama_minuman = tbNamaMinuman.Text;
                int harga_minuman = Int32.Parse(tbHargaMinuman.Text);

                if (nama_minuman == "" || filePath == "")
                {
                    MessageBox.Show("Semua field dan foto harus diisi.", "Peringatan");
                }
                else
                {
                    //Copy File
                    string directoryPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\res\";
                    string namaFile = Path.GetFileName(filePath);
                    File.Copy(filePath, directoryPath + namaFile, true);

                    //Insert to database
                    string param = "'" + nama_minuman + "', '" + harga_minuman.ToString() + "', '" + namaFile + "'";
                    Connection.tambahItem("tambah_minuman", param);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan pada Tambah Menu");
            }

            //reset field
            this.pbFotoMinuman.Image = Chickenfork_POS.Properties.Resources.food_icon;
            this.tbNamaMinuman.Text = "";
            this.tbHargaMinuman.Text = "";
            this.filePath = "";
        }
        #endregion
    }
}
