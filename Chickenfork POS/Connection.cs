﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Chickenfork_POS
{
    class Connection
    {
        #region Deklarasi variabel
        private static string connectionInfo = "" +
            "datasource=localhost; " +
            "port=3306; " +
            "username=root; " +
            "password=; " +
            "database=kasir_restoran;";
        private static MySqlConnection koneksi = new MySqlConnection(connectionInfo);
        private static MySqlCommand cmd = new MySqlCommand();

        //Metode fetch 1 --> dari slide pak Nurizal
        private static MySqlDataReader reader;

        //Metode fetch 2 --> dari internet
        private static MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
        private static DataTable result = new DataTable();
        #endregion

        #region User method
        public static string login(string username, string password, Boolean admin)
        {
            try
            {
                koneksi.Open();

                //Menentukan jenis staff
                string jenis = "kasir";
                if (admin)
                {
                    jenis = "admin";
                }

                //Membuat command 
                string query = "SELECT * FROM `tabel_staf` " +
                               "WHERE `username_staf` = '" + username + "' " +
                               "AND   `password_staf` = '" + password + "' " +
                               "AND   `jenis`         = '" + jenis    + "';" ;
                cmd.Connection = koneksi;
                cmd.CommandText = query;

                //Eksekusi command
                reader = cmd.ExecuteReader();

                //Menghitung baris
                byte row = 0;
                while (reader.Read())
                {
                    row++;
                }

                //Login true/false
                if (row == 1)
                {
                    return reader[0].ToString();
                }
                else
                {
                    MessageBox.Show("Maaf username atau password tidak sesuai. Silakan coba lagi.", "Status Login");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan Koneksi Database");
            }
            finally
            {
                reader.Close();
                koneksi.Close();
            }

            return null;
        }

        public static string getStatistikPenjualan(string nama_prosedur)
        {
            try
            {
                koneksi.Open();

                string query = "CALL " + nama_prosedur + "();";
                cmd.Connection = koneksi;
                cmd.CommandText = query;

                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan");
            }
            finally
            {
                koneksi.Close();
            }

            return "ERROR";
        }

        public static List<MenuItem> getMenuItem(string nama_prosedur, ImageList img, string jenis)
        {
            List<MenuItem> daftarMenu = new List<MenuItem>();

            try
            {
                koneksi.Open();

                string query = "CALL " + nama_prosedur + "();";
                cmd.Connection = koneksi;
                cmd.CommandText = query;

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string nama = reader[0].ToString();
                    string harga = reader[1].ToString();
                    string foto = reader[2].ToString();
                    daftarMenu.Add(new MenuItem(img, nama, harga, foto, jenis));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan");
            }
            finally
            {
                reader.Close();
                koneksi.Close();
            }

            return daftarMenu;
        }

        public static void tambahItem(string nama_prosedur, string param)
        {
            try
            {
                koneksi.Open();

                string query = "CALL " + nama_prosedur + "(" + param + ");";
                cmd.Connection = koneksi;
                cmd.CommandText = query;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Berhasil menyimpan.", "Status Simpan");
                }
                else
                {
                    MessageBox.Show("Data tidak berhasil disimpan. Silakan coba lagi.", "Status Simpan");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan");
            }
            finally
            {
                koneksi.Close();
            }
        }
        #endregion
    }
}
