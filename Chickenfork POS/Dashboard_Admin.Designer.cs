﻿namespace Chickenfork_POS
{
    partial class Dashboard_Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnTop = new System.Windows.Forms.Panel();
            this.pnLogout = new System.Windows.Forms.Panel();
            this.btLogout = new MaterialSkin.Controls.MaterialFlatButton();
            this.ctxLogout = new MaterialSkin.Controls.MaterialContextMenuStrip();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbLogoText = new System.Windows.Forms.PictureBox();
            this.pnLeftSideMenu = new System.Windows.Forms.Panel();
            this.pnMenuKelolaStaff = new System.Windows.Forms.Panel();
            this.btKelolaStaff = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnMenuTambahMinuman = new System.Windows.Forms.Panel();
            this.btTambahMinuman = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnMenuTambahMakanan = new System.Windows.Forms.Panel();
            this.btTambahMakanan = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnMenuHome = new System.Windows.Forms.Panel();
            this.btHome = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnRightBorder = new System.Windows.Forms.Panel();
            this.pnRightContainer = new System.Windows.Forms.Panel();
            this.pnTop.SuspendLayout();
            this.pnLogout.SuspendLayout();
            this.ctxLogout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoText)).BeginInit();
            this.pnLeftSideMenu.SuspendLayout();
            this.pnMenuKelolaStaff.SuspendLayout();
            this.pnMenuTambahMinuman.SuspendLayout();
            this.pnMenuTambahMakanan.SuspendLayout();
            this.pnMenuHome.SuspendLayout();
            this.pnRightBorder.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTop
            // 
            this.pnTop.Controls.Add(this.pnLogout);
            this.pnTop.Controls.Add(this.pbLogoText);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Margin = new System.Windows.Forms.Padding(2);
            this.pnTop.Name = "pnTop";
            this.pnTop.Padding = new System.Windows.Forms.Padding(13, 26, 13, 18);
            this.pnTop.Size = new System.Drawing.Size(906, 101);
            this.pnTop.TabIndex = 11;
            // 
            // pnLogout
            // 
            this.pnLogout.AutoSize = true;
            this.pnLogout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnLogout.Controls.Add(this.btLogout);
            this.pnLogout.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnLogout.Location = new System.Drawing.Point(719, 26);
            this.pnLogout.Margin = new System.Windows.Forms.Padding(2);
            this.pnLogout.Name = "pnLogout";
            this.pnLogout.Size = new System.Drawing.Size(174, 57);
            this.pnLogout.TabIndex = 5;
            // 
            // btLogout
            // 
            this.btLogout.AutoSize = true;
            this.btLogout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btLogout.ContextMenuStrip = this.ctxLogout;
            this.btLogout.Depth = 0;
            this.btLogout.Icon = global::Chickenfork_POS.Properties.Resources.logo_user;
            this.btLogout.Location = new System.Drawing.Point(13, 14);
            this.btLogout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btLogout.MouseState = MaterialSkin.MouseState.HOVER;
            this.btLogout.Name = "btLogout";
            this.btLogout.Primary = false;
            this.btLogout.Size = new System.Drawing.Size(158, 36);
            this.btLogout.TabIndex = 3;
            this.btLogout.Text = "Fikar Mukamal";
            this.btLogout.UseVisualStyleBackColor = true;
            this.btLogout.Click += new System.EventHandler(this.btLogout_Click);
            // 
            // ctxLogout
            // 
            this.ctxLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ctxLogout.Depth = 0;
            this.ctxLogout.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ctxLogout.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logOutToolStripMenuItem});
            this.ctxLogout.MouseState = MaterialSkin.MouseState.HOVER;
            this.ctxLogout.Name = "ctxLogout";
            this.ctxLogout.Size = new System.Drawing.Size(159, 38);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(158, 34);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // pbLogoText
            // 
            this.pbLogoText.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbLogoText.Image = global::Chickenfork_POS.Properties.Resources.logo_ayam_text;
            this.pbLogoText.Location = new System.Drawing.Point(13, 26);
            this.pbLogoText.Margin = new System.Windows.Forms.Padding(2);
            this.pbLogoText.Name = "pbLogoText";
            this.pbLogoText.Size = new System.Drawing.Size(240, 57);
            this.pbLogoText.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLogoText.TabIndex = 2;
            this.pbLogoText.TabStop = false;
            // 
            // pnLeftSideMenu
            // 
            this.pnLeftSideMenu.BackColor = System.Drawing.Color.White;
            this.pnLeftSideMenu.Controls.Add(this.pnMenuKelolaStaff);
            this.pnLeftSideMenu.Controls.Add(this.pnMenuTambahMinuman);
            this.pnLeftSideMenu.Controls.Add(this.pnMenuTambahMakanan);
            this.pnLeftSideMenu.Controls.Add(this.pnMenuHome);
            this.pnLeftSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnLeftSideMenu.Location = new System.Drawing.Point(0, 101);
            this.pnLeftSideMenu.Margin = new System.Windows.Forms.Padding(2);
            this.pnLeftSideMenu.Name = "pnLeftSideMenu";
            this.pnLeftSideMenu.Size = new System.Drawing.Size(327, 393);
            this.pnLeftSideMenu.TabIndex = 13;
            // 
            // pnMenuKelolaStaff
            // 
            this.pnMenuKelolaStaff.BackColor = System.Drawing.Color.White;
            this.pnMenuKelolaStaff.Controls.Add(this.btKelolaStaff);
            this.pnMenuKelolaStaff.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMenuKelolaStaff.Location = new System.Drawing.Point(0, 156);
            this.pnMenuKelolaStaff.Margin = new System.Windows.Forms.Padding(2);
            this.pnMenuKelolaStaff.Name = "pnMenuKelolaStaff";
            this.pnMenuKelolaStaff.Size = new System.Drawing.Size(327, 52);
            this.pnMenuKelolaStaff.TabIndex = 3;
            // 
            // btKelolaStaff
            // 
            this.btKelolaStaff.AutoSize = true;
            this.btKelolaStaff.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btKelolaStaff.Depth = 0;
            this.btKelolaStaff.Icon = null;
            this.btKelolaStaff.Location = new System.Drawing.Point(0, 0);
            this.btKelolaStaff.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btKelolaStaff.MinimumSize = new System.Drawing.Size(327, 52);
            this.btKelolaStaff.MouseState = MaterialSkin.MouseState.HOVER;
            this.btKelolaStaff.Name = "btKelolaStaff";
            this.btKelolaStaff.Primary = false;
            this.btKelolaStaff.Size = new System.Drawing.Size(327, 52);
            this.btKelolaStaff.TabIndex = 0;
            this.btKelolaStaff.Text = "Kelola Staff";
            this.btKelolaStaff.UseVisualStyleBackColor = true;
            this.btKelolaStaff.Click += new System.EventHandler(this.sideMenu_Click);
            // 
            // pnMenuTambahMinuman
            // 
            this.pnMenuTambahMinuman.Controls.Add(this.btTambahMinuman);
            this.pnMenuTambahMinuman.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMenuTambahMinuman.Location = new System.Drawing.Point(0, 104);
            this.pnMenuTambahMinuman.Margin = new System.Windows.Forms.Padding(2);
            this.pnMenuTambahMinuman.Name = "pnMenuTambahMinuman";
            this.pnMenuTambahMinuman.Size = new System.Drawing.Size(327, 52);
            this.pnMenuTambahMinuman.TabIndex = 2;
            // 
            // btTambahMinuman
            // 
            this.btTambahMinuman.AutoSize = true;
            this.btTambahMinuman.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btTambahMinuman.Depth = 0;
            this.btTambahMinuman.Icon = null;
            this.btTambahMinuman.Location = new System.Drawing.Point(0, 0);
            this.btTambahMinuman.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btTambahMinuman.MinimumSize = new System.Drawing.Size(327, 52);
            this.btTambahMinuman.MouseState = MaterialSkin.MouseState.HOVER;
            this.btTambahMinuman.Name = "btTambahMinuman";
            this.btTambahMinuman.Primary = false;
            this.btTambahMinuman.Size = new System.Drawing.Size(327, 52);
            this.btTambahMinuman.TabIndex = 0;
            this.btTambahMinuman.Text = "Tambah Menu Minuman";
            this.btTambahMinuman.UseVisualStyleBackColor = true;
            this.btTambahMinuman.Click += new System.EventHandler(this.sideMenu_Click);
            // 
            // pnMenuTambahMakanan
            // 
            this.pnMenuTambahMakanan.Controls.Add(this.btTambahMakanan);
            this.pnMenuTambahMakanan.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMenuTambahMakanan.Location = new System.Drawing.Point(0, 52);
            this.pnMenuTambahMakanan.Margin = new System.Windows.Forms.Padding(2);
            this.pnMenuTambahMakanan.Name = "pnMenuTambahMakanan";
            this.pnMenuTambahMakanan.Size = new System.Drawing.Size(327, 52);
            this.pnMenuTambahMakanan.TabIndex = 1;
            // 
            // btTambahMakanan
            // 
            this.btTambahMakanan.AutoSize = true;
            this.btTambahMakanan.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btTambahMakanan.Depth = 0;
            this.btTambahMakanan.Icon = null;
            this.btTambahMakanan.Location = new System.Drawing.Point(0, 0);
            this.btTambahMakanan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btTambahMakanan.MinimumSize = new System.Drawing.Size(327, 52);
            this.btTambahMakanan.MouseState = MaterialSkin.MouseState.HOVER;
            this.btTambahMakanan.Name = "btTambahMakanan";
            this.btTambahMakanan.Primary = false;
            this.btTambahMakanan.Size = new System.Drawing.Size(327, 52);
            this.btTambahMakanan.TabIndex = 0;
            this.btTambahMakanan.Text = "Tambah Menu Makanan";
            this.btTambahMakanan.UseVisualStyleBackColor = true;
            this.btTambahMakanan.Click += new System.EventHandler(this.sideMenu_Click);
            // 
            // pnMenuHome
            // 
            this.pnMenuHome.Controls.Add(this.btHome);
            this.pnMenuHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnMenuHome.Location = new System.Drawing.Point(0, 0);
            this.pnMenuHome.Margin = new System.Windows.Forms.Padding(2);
            this.pnMenuHome.Name = "pnMenuHome";
            this.pnMenuHome.Size = new System.Drawing.Size(327, 52);
            this.pnMenuHome.TabIndex = 0;
            // 
            // btHome
            // 
            this.btHome.AutoSize = true;
            this.btHome.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btHome.Depth = 0;
            this.btHome.Icon = null;
            this.btHome.Location = new System.Drawing.Point(0, 0);
            this.btHome.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btHome.MinimumSize = new System.Drawing.Size(327, 52);
            this.btHome.MouseState = MaterialSkin.MouseState.HOVER;
            this.btHome.Name = "btHome";
            this.btHome.Primary = false;
            this.btHome.Size = new System.Drawing.Size(327, 52);
            this.btHome.TabIndex = 0;
            this.btHome.Text = "Home";
            this.btHome.UseVisualStyleBackColor = true;
            this.btHome.Click += new System.EventHandler(this.sideMenu_Click);
            // 
            // pnRightBorder
            // 
            this.pnRightBorder.BackColor = System.Drawing.Color.Silver;
            this.pnRightBorder.Controls.Add(this.pnRightContainer);
            this.pnRightBorder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRightBorder.Location = new System.Drawing.Point(327, 101);
            this.pnRightBorder.Margin = new System.Windows.Forms.Padding(2);
            this.pnRightBorder.Name = "pnRightBorder";
            this.pnRightBorder.Padding = new System.Windows.Forms.Padding(5);
            this.pnRightBorder.Size = new System.Drawing.Size(579, 393);
            this.pnRightBorder.TabIndex = 0;
            // 
            // pnRightContainer
            // 
            this.pnRightContainer.BackColor = System.Drawing.Color.White;
            this.pnRightContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnRightContainer.Location = new System.Drawing.Point(5, 5);
            this.pnRightContainer.Margin = new System.Windows.Forms.Padding(2);
            this.pnRightContainer.Name = "pnRightContainer";
            this.pnRightContainer.Size = new System.Drawing.Size(569, 383);
            this.pnRightContainer.TabIndex = 0;
            // 
            // Dashboard_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(919, 507);
            this.Controls.Add(this.pnRightBorder);
            this.Controls.Add(this.pnLeftSideMenu);
            this.Controls.Add(this.pnTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Dashboard_Admin";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 13, 13);
            this.ShowInTaskbar = false;
            this.Text = "Dashboard_Administrator";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Dashboard_Administrator_Load);
            this.pnTop.ResumeLayout(false);
            this.pnTop.PerformLayout();
            this.pnLogout.ResumeLayout(false);
            this.pnLogout.PerformLayout();
            this.ctxLogout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoText)).EndInit();
            this.pnLeftSideMenu.ResumeLayout(false);
            this.pnMenuKelolaStaff.ResumeLayout(false);
            this.pnMenuKelolaStaff.PerformLayout();
            this.pnMenuTambahMinuman.ResumeLayout(false);
            this.pnMenuTambahMinuman.PerformLayout();
            this.pnMenuTambahMakanan.ResumeLayout(false);
            this.pnMenuTambahMakanan.PerformLayout();
            this.pnMenuHome.ResumeLayout(false);
            this.pnMenuHome.PerformLayout();
            this.pnRightBorder.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnTop;
        private System.Windows.Forms.PictureBox pbLogoText;
        private System.Windows.Forms.Panel pnLeftSideMenu;
        private System.Windows.Forms.Panel pnRightBorder;
        private System.Windows.Forms.Panel pnRightContainer;
        private System.Windows.Forms.Panel pnLogout;
        private MaterialSkin.Controls.MaterialFlatButton btLogout;
        private MaterialSkin.Controls.MaterialContextMenuStrip ctxLogout;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.Panel pnMenuHome;
        private MaterialSkin.Controls.MaterialFlatButton btHome;
        private System.Windows.Forms.Panel pnMenuKelolaStaff;
        private MaterialSkin.Controls.MaterialFlatButton btKelolaStaff;
        private System.Windows.Forms.Panel pnMenuTambahMinuman;
        private MaterialSkin.Controls.MaterialFlatButton btTambahMinuman;
        private System.Windows.Forms.Panel pnMenuTambahMakanan;
        private MaterialSkin.Controls.MaterialFlatButton btTambahMakanan;
    }
}