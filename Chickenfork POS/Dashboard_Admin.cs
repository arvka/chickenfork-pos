﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Chickenfork_POS
{
    public partial class Dashboard_Admin : Form
    {
        #region Deklarasi variabel
        private Form frOpened;
        private Panel pnFocused;
        private Button btFocused;
        private String usernameAkun;
        private Form_Login formLogin;
        private Admin_Home formHome;
        private Admin_KelolaAkun formKelolaStaff;
        private Admin_TambahMenuMakanan formTambahMakanan;
        private Admin_TambahMenuMinuman formTambahMinuman;
        #endregion

        #region Constructor
        public Dashboard_Admin(Form_Login _formLogin)
        {
            InitializeComponent();
            formLogin = _formLogin;
            usernameAkun = _formLogin.usernameAkun;
        }
        #endregion
        
        #region User method
        public void backToMenuHome()
        {
            this.btHome.PerformClick();
        }
        #endregion

        #region Event handler method
        private void Dashboard_Administrator_Load(object sender, EventArgs e)
        {
            this.pnFocused = this.pnMenuHome;
            this.btFocused = this.btHome;
            this.frOpened = null;
            
            this.btHome.PerformClick();
            this.btLogout.Text = usernameAkun;
        }

        private void btLogout_Click(object sender, EventArgs e)
        {
            Point screenPoint = btLogout.PointToScreen(new Point(btLogout.Left, btLogout.Bottom));
            if (screenPoint.Y + ctxLogout.Size.Height > Screen.PrimaryScreen.WorkingArea.Height)
            {
                ctxLogout.Show(btLogout, new Point(0, -ctxLogout.Size.Height));
            }
            else
            {
                ctxLogout.Show(btLogout, new Point(0, btLogout.Height));
            }
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Anda yakin ingin logout?", "Konfirmasi Logout", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void sideMenu_Click(object sender, EventArgs e)
        {
            btFocused = (Button)sender;
            pnFocused.BackColor = System.Drawing.Color.White;
            pnRightContainer.Controls.Remove(frOpened);

            if (btFocused.Equals(this.btHome))
            {
                frOpened = new Admin_Home();
                pnFocused = this.pnMenuHome;
            }
            else if (btFocused.Equals(this.btTambahMakanan))
            {
                frOpened = new Admin_TambahMenuMakanan();
                pnFocused = this.pnMenuTambahMakanan;
            }
            else if (btFocused.Equals(this.btTambahMinuman))
            {
                frOpened = new Admin_TambahMenuMinuman();
                pnFocused = this.pnMenuTambahMinuman;
            }
            else if (btFocused.Equals(this.btKelolaStaff))
            {
                frOpened = new Admin_KelolaAkun();
                pnFocused = this.pnMenuKelolaStaff;
            }

            pnFocused.BackColor = System.Drawing.Color.Silver;
            frOpened.TopLevel = false;
            frOpened.Dock = DockStyle.Fill;
            pnRightContainer.Controls.Add(frOpened);
            frOpened.Show();
        }
        #endregion
    }
}
