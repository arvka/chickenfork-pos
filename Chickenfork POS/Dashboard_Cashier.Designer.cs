﻿namespace Chickenfork_POS
{
    partial class Dashboard_Cashier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard_Cashier));
            this.containerTop = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btLogout = new MaterialSkin.Controls.MaterialFlatButton();
            this.ctxLogout = new MaterialSkin.Controls.MaterialContextMenuStrip();
            this.logOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbLogoText = new System.Windows.Forms.PictureBox();
            this.scFill = new System.Windows.Forms.SplitContainer();
            this.dgvTabel = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnBtLogin = new System.Windows.Forms.Panel();
            this.btBayar = new MaterialSkin.Controls.MaterialFlatButton();
            this.label2 = new System.Windows.Forms.Label();
            this.lbTotalHarga = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lbOrderNo = new System.Windows.Forms.Label();
            this.btNext = new MaterialSkin.Controls.MaterialFlatButton();
            this.btPrev = new MaterialSkin.Controls.MaterialFlatButton();
            this.tcMenu = new MetroFramework.Controls.MetroTabControl();
            this.tpMenuMakanan = new MetroFramework.Controls.MetroTabPage();
            this.flpMenuMakanan = new System.Windows.Forms.FlowLayoutPanel();
            this.tpMenuMinuman = new MetroFramework.Controls.MetroTabPage();
            this.flpMenuMinuman = new System.Windows.Forms.FlowLayoutPanel();
            this.pnContainerFill = new System.Windows.Forms.Panel();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.containerTop.SuspendLayout();
            this.panel3.SuspendLayout();
            this.ctxLogout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.scFill)).BeginInit();
            this.scFill.Panel1.SuspendLayout();
            this.scFill.Panel2.SuspendLayout();
            this.scFill.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabel)).BeginInit();
            this.panel6.SuspendLayout();
            this.pnBtLogin.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tcMenu.SuspendLayout();
            this.tpMenuMakanan.SuspendLayout();
            this.tpMenuMinuman.SuspendLayout();
            this.pnContainerFill.SuspendLayout();
            this.SuspendLayout();
            // 
            // containerTop
            // 
            this.containerTop.Controls.Add(this.panel3);
            this.containerTop.Controls.Add(this.pbLogoText);
            this.containerTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.containerTop.Location = new System.Drawing.Point(0, 0);
            this.containerTop.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.containerTop.Name = "containerTop";
            this.containerTop.Padding = new System.Windows.Forms.Padding(13, 26, 13, 6);
            this.containerTop.Size = new System.Drawing.Size(919, 90);
            this.containerTop.TabIndex = 8;
            // 
            // panel3
            // 
            this.panel3.AutoSize = true;
            this.panel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel3.Controls.Add(this.btLogout);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(732, 26);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(174, 58);
            this.panel3.TabIndex = 4;
            // 
            // btLogout
            // 
            this.btLogout.AutoSize = true;
            this.btLogout.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btLogout.ContextMenuStrip = this.ctxLogout;
            this.btLogout.Depth = 0;
            this.btLogout.Icon = global::Chickenfork_POS.Properties.Resources.logo_user;
            this.btLogout.Location = new System.Drawing.Point(13, 14);
            this.btLogout.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btLogout.MouseState = MaterialSkin.MouseState.HOVER;
            this.btLogout.Name = "btLogout";
            this.btLogout.Primary = false;
            this.btLogout.Size = new System.Drawing.Size(158, 36);
            this.btLogout.TabIndex = 3;
            this.btLogout.Text = "Fikar Mukamal";
            this.btLogout.UseVisualStyleBackColor = true;
            this.btLogout.Click += new System.EventHandler(this.btLogout_Click);
            // 
            // ctxLogout
            // 
            this.ctxLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ctxLogout.Depth = 0;
            this.ctxLogout.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ctxLogout.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logOutToolStripMenuItem});
            this.ctxLogout.MouseState = MaterialSkin.MouseState.HOVER;
            this.ctxLogout.Name = "ctxLogout";
            this.ctxLogout.Size = new System.Drawing.Size(159, 38);
            // 
            // logOutToolStripMenuItem
            // 
            this.logOutToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logOutToolStripMenuItem.Name = "logOutToolStripMenuItem";
            this.logOutToolStripMenuItem.Size = new System.Drawing.Size(158, 34);
            this.logOutToolStripMenuItem.Text = "Log Out";
            this.logOutToolStripMenuItem.Click += new System.EventHandler(this.logOutToolStripMenuItem_Click);
            // 
            // pbLogoText
            // 
            this.pbLogoText.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbLogoText.Image = global::Chickenfork_POS.Properties.Resources.logo_ayam_text;
            this.pbLogoText.Location = new System.Drawing.Point(13, 26);
            this.pbLogoText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbLogoText.Name = "pbLogoText";
            this.pbLogoText.Size = new System.Drawing.Size(240, 58);
            this.pbLogoText.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbLogoText.TabIndex = 2;
            this.pbLogoText.TabStop = false;
            // 
            // scFill
            // 
            this.scFill.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.scFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scFill.Location = new System.Drawing.Point(13, 13);
            this.scFill.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.scFill.Name = "scFill";
            // 
            // scFill.Panel1
            // 
            this.scFill.Panel1.BackColor = System.Drawing.Color.White;
            this.scFill.Panel1.Controls.Add(this.dgvTabel);
            this.scFill.Panel1.Controls.Add(this.panel6);
            this.scFill.Panel1.Controls.Add(this.panel4);
            this.scFill.Panel1.Padding = new System.Windows.Forms.Padding(27, 13, 60, 13);
            this.scFill.Panel1MinSize = 450;
            // 
            // scFill.Panel2
            // 
            this.scFill.Panel2.AutoScroll = true;
            this.scFill.Panel2.BackColor = System.Drawing.Color.White;
            this.scFill.Panel2.Controls.Add(this.tcMenu);
            this.scFill.Panel2.Padding = new System.Windows.Forms.Padding(47, 13, 27, 0);
            this.scFill.Panel2MinSize = 350;
            this.scFill.Size = new System.Drawing.Size(893, 398);
            this.scFill.SplitterDistance = 450;
            this.scFill.SplitterWidth = 3;
            this.scFill.TabIndex = 1;
            // 
            // dgvTabel
            // 
            this.dgvTabel.AllowUserToAddRows = false;
            this.dgvTabel.AllowUserToDeleteRows = false;
            this.dgvTabel.AllowUserToResizeRows = false;
            this.dgvTabel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTabel.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvTabel.BackgroundColor = System.Drawing.Color.White;
            this.dgvTabel.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvTabel.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvTabel.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(10);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTabel.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTabel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTabel.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvTabel.EnableHeadersVisualStyles = false;
            this.dgvTabel.Location = new System.Drawing.Point(27, 66);
            this.dgvTabel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvTabel.MultiSelect = false;
            this.dgvTabel.Name = "dgvTabel";
            this.dgvTabel.ReadOnly = true;
            this.dgvTabel.RowHeadersVisible = false;
            this.dgvTabel.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvTabel.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.dgvTabel.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
            this.dgvTabel.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvTabel.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.DimGray;
            this.dgvTabel.RowTemplate.DefaultCellStyle.Padding = new System.Windows.Forms.Padding(10);
            this.dgvTabel.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
            this.dgvTabel.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.DimGray;
            this.dgvTabel.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTabel.RowTemplate.Height = 50;
            this.dgvTabel.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTabel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTabel.ShowCellToolTips = false;
            this.dgvTabel.ShowEditingIcon = false;
            this.dgvTabel.Size = new System.Drawing.Size(363, 203);
            this.dgvTabel.TabIndex = 6;
            this.dgvTabel.TabStop = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pnBtLogin);
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.lbTotalHarga);
            this.panel6.Controls.Add(this.label3);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(27, 269);
            this.panel6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(363, 116);
            this.panel6.TabIndex = 5;
            // 
            // pnBtLogin
            // 
            this.pnBtLogin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnBtLogin.BackColor = System.Drawing.Color.White;
            this.pnBtLogin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnBtLogin.Controls.Add(this.btBayar);
            this.pnBtLogin.Location = new System.Drawing.Point(202, 58);
            this.pnBtLogin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnBtLogin.Name = "pnBtLogin";
            this.pnBtLogin.Size = new System.Drawing.Size(181, 42);
            this.pnBtLogin.TabIndex = 14;
            // 
            // btBayar
            // 
            this.btBayar.AutoSize = true;
            this.btBayar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btBayar.Depth = 0;
            this.btBayar.ForeColor = System.Drawing.Color.White;
            this.btBayar.Icon = null;
            this.btBayar.Location = new System.Drawing.Point(-1, -1);
            this.btBayar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btBayar.MinimumSize = new System.Drawing.Size(181, 42);
            this.btBayar.MouseState = MaterialSkin.MouseState.HOVER;
            this.btBayar.Name = "btBayar";
            this.btBayar.Primary = false;
            this.btBayar.Size = new System.Drawing.Size(181, 42);
            this.btBayar.TabIndex = 9;
            this.btBayar.Text = "Bayar Sekarang";
            this.btBayar.UseVisualStyleBackColor = true;
            this.btBayar.Click += new System.EventHandler(this.btBayar_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.label2.Location = new System.Drawing.Point(-1, 38);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 23);
            this.label2.TabIndex = 13;
            this.label2.Text = "Total";
            // 
            // lbTotalHarga
            // 
            this.lbTotalHarga.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbTotalHarga.AutoSize = true;
            this.lbTotalHarga.Font = new System.Drawing.Font("Calibri", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalHarga.Location = new System.Drawing.Point(47, 56);
            this.lbTotalHarga.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTotalHarga.Name = "lbTotalHarga";
            this.lbTotalHarga.Size = new System.Drawing.Size(39, 46);
            this.lbTotalHarga.TabIndex = 12;
            this.lbTotalHarga.Text = "0";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 28F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-7, 56);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 46);
            this.label3.TabIndex = 1;
            this.label3.Text = "Rp";
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(27, 13);
            this.panel4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(363, 53);
            this.panel4.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel5.AutoSize = true;
            this.panel5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel5.Controls.Add(this.lbOrderNo);
            this.panel5.Controls.Add(this.btNext);
            this.panel5.Controls.Add(this.btPrev);
            this.panel5.Location = new System.Drawing.Point(12, 2);
            this.panel5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(343, 49);
            this.panel5.TabIndex = 3;
            this.panel5.Visible = false;
            // 
            // lbOrderNo
            // 
            this.lbOrderNo.AutoSize = true;
            this.lbOrderNo.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.lbOrderNo.Location = new System.Drawing.Point(105, 12);
            this.lbOrderNo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbOrderNo.Name = "lbOrderNo";
            this.lbOrderNo.Size = new System.Drawing.Size(133, 23);
            this.lbOrderNo.TabIndex = 5;
            this.lbOrderNo.Text = "Order No. 1234";
            // 
            // btNext
            // 
            this.btNext.AutoSize = true;
            this.btNext.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btNext.Depth = 0;
            this.btNext.Icon = null;
            this.btNext.Location = new System.Drawing.Point(285, 9);
            this.btNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btNext.MouseState = MaterialSkin.MouseState.HOVER;
            this.btNext.Name = "btNext";
            this.btNext.Primary = false;
            this.btNext.Size = new System.Drawing.Size(55, 36);
            this.btNext.TabIndex = 4;
            this.btNext.Text = "Next";
            this.btNext.UseVisualStyleBackColor = true;
            // 
            // btPrev
            // 
            this.btPrev.AutoSize = true;
            this.btPrev.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btPrev.Depth = 0;
            this.btPrev.Icon = null;
            this.btPrev.Location = new System.Drawing.Point(9, 9);
            this.btPrev.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btPrev.MouseState = MaterialSkin.MouseState.HOVER;
            this.btPrev.Name = "btPrev";
            this.btPrev.Primary = false;
            this.btPrev.Size = new System.Drawing.Size(55, 36);
            this.btPrev.TabIndex = 3;
            this.btPrev.Text = "Prev";
            this.btPrev.UseVisualStyleBackColor = true;
            // 
            // tcMenu
            // 
            this.tcMenu.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tcMenu.Controls.Add(this.tpMenuMakanan);
            this.tcMenu.Controls.Add(this.tpMenuMinuman);
            this.tcMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcMenu.FontSize = MetroFramework.MetroTabControlSize.Tall;
            this.tcMenu.FontWeight = MetroFramework.MetroTabControlWeight.Regular;
            this.tcMenu.Location = new System.Drawing.Point(47, 13);
            this.tcMenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tcMenu.Name = "tcMenu";
            this.tcMenu.SelectedIndex = 0;
            this.tcMenu.Size = new System.Drawing.Size(366, 385);
            this.tcMenu.Style = MetroFramework.MetroColorStyle.Red;
            this.tcMenu.TabIndex = 17;
            this.tcMenu.TabStop = false;
            this.tcMenu.UseSelectable = true;
            // 
            // tpMenuMakanan
            // 
            this.tpMenuMakanan.Controls.Add(this.flpMenuMakanan);
            this.tpMenuMakanan.HorizontalScrollbarBarColor = false;
            this.tpMenuMakanan.HorizontalScrollbarHighlightOnWheel = false;
            this.tpMenuMakanan.HorizontalScrollbarSize = 6;
            this.tpMenuMakanan.Location = new System.Drawing.Point(4, 47);
            this.tpMenuMakanan.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpMenuMakanan.Name = "tpMenuMakanan";
            this.tpMenuMakanan.Padding = new System.Windows.Forms.Padding(13, 13, 13, 0);
            this.tpMenuMakanan.Size = new System.Drawing.Size(358, 334);
            this.tpMenuMakanan.TabIndex = 0;
            this.tpMenuMakanan.Text = "Menu Makanan";
            this.tpMenuMakanan.VerticalScrollbarBarColor = true;
            this.tpMenuMakanan.VerticalScrollbarHighlightOnWheel = false;
            this.tpMenuMakanan.VerticalScrollbarSize = 7;
            // 
            // flpMenuMakanan
            // 
            this.flpMenuMakanan.AutoScroll = true;
            this.flpMenuMakanan.AutoSize = true;
            this.flpMenuMakanan.BackColor = System.Drawing.Color.White;
            this.flpMenuMakanan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpMenuMakanan.Location = new System.Drawing.Point(13, 13);
            this.flpMenuMakanan.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.flpMenuMakanan.Name = "flpMenuMakanan";
            this.flpMenuMakanan.Size = new System.Drawing.Size(332, 321);
            this.flpMenuMakanan.TabIndex = 1;
            // 
            // tpMenuMinuman
            // 
            this.tpMenuMinuman.Controls.Add(this.flpMenuMinuman);
            this.tpMenuMinuman.HorizontalScrollbarBarColor = true;
            this.tpMenuMinuman.HorizontalScrollbarHighlightOnWheel = false;
            this.tpMenuMinuman.HorizontalScrollbarSize = 6;
            this.tpMenuMinuman.Location = new System.Drawing.Point(4, 47);
            this.tpMenuMinuman.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tpMenuMinuman.Name = "tpMenuMinuman";
            this.tpMenuMinuman.Padding = new System.Windows.Forms.Padding(13, 13, 13, 0);
            this.tpMenuMinuman.Size = new System.Drawing.Size(537, 472);
            this.tpMenuMinuman.TabIndex = 1;
            this.tpMenuMinuman.Text = "Menu Minuman";
            this.tpMenuMinuman.VerticalScrollbarBarColor = true;
            this.tpMenuMinuman.VerticalScrollbarHighlightOnWheel = false;
            this.tpMenuMinuman.VerticalScrollbarSize = 7;
            // 
            // flpMenuMinuman
            // 
            this.flpMenuMinuman.BackColor = System.Drawing.Color.White;
            this.flpMenuMinuman.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpMenuMinuman.Location = new System.Drawing.Point(13, 13);
            this.flpMenuMinuman.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.flpMenuMinuman.Name = "flpMenuMinuman";
            this.flpMenuMinuman.Size = new System.Drawing.Size(511, 459);
            this.flpMenuMinuman.TabIndex = 2;
            // 
            // pnContainerFill
            // 
            this.pnContainerFill.Controls.Add(this.scFill);
            this.pnContainerFill.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainerFill.Location = new System.Drawing.Point(0, 90);
            this.pnContainerFill.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnContainerFill.Name = "pnContainerFill";
            this.pnContainerFill.Padding = new System.Windows.Forms.Padding(13, 13, 13, 6);
            this.pnContainerFill.Size = new System.Drawing.Size(919, 417);
            this.pnContainerFill.TabIndex = 10;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "food_icon.png");
            this.imageList.Images.SetKeyName(1, "logo_user.png");
            // 
            // Dashboard_Cashier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(919, 507);
            this.Controls.Add(this.pnContainerFill);
            this.Controls.Add(this.containerTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MinimumSize = new System.Drawing.Size(909, 472);
            this.Name = "Dashboard_Cashier";
            this.ShowInTaskbar = false;
            this.Text = "Dashboard_Cashier";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Dashboard_Cashier_Load);
            this.containerTop.ResumeLayout(false);
            this.containerTop.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ctxLogout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogoText)).EndInit();
            this.scFill.Panel1.ResumeLayout(false);
            this.scFill.Panel1.PerformLayout();
            this.scFill.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scFill)).EndInit();
            this.scFill.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTabel)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.pnBtLogin.ResumeLayout(false);
            this.pnBtLogin.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tcMenu.ResumeLayout(false);
            this.tpMenuMakanan.ResumeLayout(false);
            this.tpMenuMakanan.PerformLayout();
            this.tpMenuMinuman.ResumeLayout(false);
            this.pnContainerFill.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel containerTop;
        private MaterialSkin.Controls.MaterialFlatButton btLogout;
        private System.Windows.Forms.PictureBox pbLogoText;
        private MaterialSkin.Controls.MaterialContextMenuStrip ctxLogout;
        private System.Windows.Forms.ToolStripMenuItem logOutToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer scFill;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbTotalHarga;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbOrderNo;
        private MaterialSkin.Controls.MaterialFlatButton btNext;
        private MaterialSkin.Controls.MaterialFlatButton btPrev;
        private System.Windows.Forms.FlowLayoutPanel flpMenuMakanan;
        private MetroFramework.Controls.MetroTabControl tcMenu;
        private MetroFramework.Controls.MetroTabPage tpMenuMakanan;
        private MetroFramework.Controls.MetroTabPage tpMenuMinuman;
        private System.Windows.Forms.Panel pnContainerFill;
        private System.Windows.Forms.FlowLayoutPanel flpMenuMinuman;
        private System.Windows.Forms.DataGridView dgvTabel;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Panel pnBtLogin;
        private MaterialSkin.Controls.MaterialFlatButton btBayar;
    }
}