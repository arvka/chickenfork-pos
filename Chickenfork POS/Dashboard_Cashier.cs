﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Chickenfork_POS
{
    public partial class Dashboard_Cashier : Form
    {
        #region Deklarasi variabel
        private JumlahPesan formJumlahPesan;
        private Form_Login formLogin;
        private int countSubTotal = 0;
        private int countMakanan = 0;
        private int countMinuman = 0;

        public DataTable dt;
        public int currentRow = 0;
        #endregion

        #region Constructor
        public Dashboard_Cashier(Form_Login _formLogin)
        {
            InitializeComponent();
            this.formLogin = _formLogin;
            this.btLogout.Text = formLogin.usernameAkun;
        }
        #endregion

        #region User method
        private void tampilkanDaftarMenu()
        {
            //Menampilkan Menu Makanan
            List<MenuItem> itemList = Connection.getMenuItem("daftar_makanan", this.imageList, "makanan");
            foreach (MenuItem item in itemList)
            {
                item.ButtonClick += new System.EventHandler(this.menu_Click);
                flpMenuMakanan.Controls.Add(item);
            }

            //Menampilkan Menu Minuman
            itemList = Connection.getMenuItem("daftar_minuman", this.imageList, "minuman");
            foreach (MenuItem item in itemList)
            {
                item.ButtonClick += new System.EventHandler(this.menu_Click);
                flpMenuMinuman.Controls.Add(item);
            }
        }

        public void hitungTotalHarga()
        {
            int jumlah = Convert.ToInt32(dgvTabel.Rows[currentRow].Cells[2].Value);
            countSubTotal += Convert.ToInt32(dgvTabel.Rows[currentRow].Cells[4].Value);
            if (dgvTabel.Rows[currentRow].Cells[1].Value == "makanan")
            {
                countMakanan += jumlah;
            }
            else
            {
                countMinuman += jumlah;
            }
            lbTotalHarga.Text = countSubTotal.ToString();
        }
        #endregion

        #region Event handler method
        private void Dashboard_Cashier_Load(object sender, EventArgs e)
        {
            //Menyiapkan kolom
            dt = new DataTable();
            dt.Columns.Add("Menu".ToString());
            dt.Columns.Add("Jenis".ToString());
            dt.Columns.Add("Jumlah".ToString());
            dt.Columns.Add("Harga/porsi".ToString());
            dt.Columns.Add("Sub Total".ToString());

            //Memasukkan DataTable ke DataGridView
            dgvTabel.DataSource = dt;
            
            //Menampilkan menu makanan dan minuman
            tampilkanDaftarMenu();

            formJumlahPesan = new JumlahPesan(this);
        }

        private void btLogout_Click(object sender, EventArgs e)
        {
            Point screenPoint = btLogout.PointToScreen(new Point(btLogout.Left, btLogout.Bottom));
            if (screenPoint.Y + ctxLogout.Size.Height > Screen.PrimaryScreen.WorkingArea.Height)
            {
                ctxLogout.Show(btLogout, new Point(0, -ctxLogout.Size.Height));
            }
            else
            {
                ctxLogout.Show(btLogout, new Point(0, btLogout.Height));
            }
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Anda yakin ingin logout?", "Konfirmasi Logout", MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                formLogin.Show();
                this.Close();
            }
        }

        private void menu_Click(object sender, EventArgs e)
        {
            MenuItem item = ((MenuItem)sender);
            formJumlahPesan.setVariabel(item.nama, item.harga, item.jenis);
            formJumlahPesan.ShowDialog();
        }
        
        private void btBayar_Click(object sender, EventArgs e)
        {
            string param = "NULL, '" + 
                countMakanan + "', '" + 
                countMinuman + "', '" + 
                countSubTotal + "'";
            Connection.tambahItem("tambah_pemesanan", param);

            //reset table dan total harga
            this.dt.Rows.Clear();
            this.lbTotalHarga.Text = "0";
        }
        #endregion
    }
}
