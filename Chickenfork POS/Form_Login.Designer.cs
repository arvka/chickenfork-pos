﻿namespace Chickenfork_POS
{
    partial class Form_Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnCenter = new System.Windows.Forms.Panel();
            this.btLogin = new MaterialSkin.Controls.MaterialRaisedButton();
            this.cbAdmin = new MetroFramework.Controls.MetroCheckBox();
            this.pnPassword = new System.Windows.Forms.Panel();
            this.pbPassword = new System.Windows.Forms.PictureBox();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.pnUsername = new System.Windows.Forms.Panel();
            this.pbUsername = new System.Windows.Forms.PictureBox();
            this.tbUsername = new System.Windows.Forms.TextBox();
            this.lbSilahkan = new System.Windows.Forms.Label();
            this.lbWelcome = new System.Windows.Forms.Label();
            this.logo_ayam = new System.Windows.Forms.PictureBox();
            this.lnKeluar = new System.Windows.Forms.LinkLabel();
            this.pnBottom = new System.Windows.Forms.Panel();
            this.pnContainer = new System.Windows.Forms.Panel();
            this.pnCenter.SuspendLayout();
            this.pnPassword.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPassword)).BeginInit();
            this.pnUsername.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUsername)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo_ayam)).BeginInit();
            this.pnBottom.SuspendLayout();
            this.pnContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnCenter
            // 
            this.pnCenter.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnCenter.Controls.Add(this.btLogin);
            this.pnCenter.Controls.Add(this.cbAdmin);
            this.pnCenter.Controls.Add(this.pnPassword);
            this.pnCenter.Controls.Add(this.pnUsername);
            this.pnCenter.Controls.Add(this.lbSilahkan);
            this.pnCenter.Controls.Add(this.lbWelcome);
            this.pnCenter.Controls.Add(this.logo_ayam);
            this.pnCenter.Location = new System.Drawing.Point(208, 220);
            this.pnCenter.Name = "pnCenter";
            this.pnCenter.Size = new System.Drawing.Size(646, 644);
            this.pnCenter.TabIndex = 1;
            // 
            // btLogin
            // 
            this.btLogin.AutoSize = true;
            this.btLogin.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btLogin.Depth = 0;
            this.btLogin.Icon = null;
            this.btLogin.Location = new System.Drawing.Point(87, 521);
            this.btLogin.MinimumSize = new System.Drawing.Size(472, 66);
            this.btLogin.MouseState = MaterialSkin.MouseState.HOVER;
            this.btLogin.Name = "btLogin";
            this.btLogin.Primary = true;
            this.btLogin.Size = new System.Drawing.Size(472, 66);
            this.btLogin.TabIndex = 3;
            this.btLogin.Text = "Login";
            this.btLogin.UseVisualStyleBackColor = true;
            this.btLogin.Click += new System.EventHandler(this.btLogin_Click);
            // 
            // cbAdmin
            // 
            this.cbAdmin.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbAdmin.AutoSize = true;
            this.cbAdmin.FontSize = MetroFramework.MetroCheckBoxSize.Tall;
            this.cbAdmin.Location = new System.Drawing.Point(174, 605);
            this.cbAdmin.Name = "cbAdmin";
            this.cbAdmin.Size = new System.Drawing.Size(204, 25);
            this.cbAdmin.Style = MetroFramework.MetroColorStyle.Red;
            this.cbAdmin.TabIndex = 4;
            this.cbAdmin.Text = "Masuk sebagai Admin";
            this.cbAdmin.UseSelectable = true;
            // 
            // pnPassword
            // 
            this.pnPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnPassword.Controls.Add(this.pbPassword);
            this.pnPassword.Controls.Add(this.tbPassword);
            this.pnPassword.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnPassword.Location = new System.Drawing.Point(87, 437);
            this.pnPassword.Name = "pnPassword";
            this.pnPassword.Size = new System.Drawing.Size(472, 65);
            this.pnPassword.TabIndex = 8;
            // 
            // pbPassword
            // 
            this.pbPassword.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbPassword.Image = global::Chickenfork_POS.Properties.Resources.logo_password;
            this.pbPassword.Location = new System.Drawing.Point(0, 0);
            this.pbPassword.Name = "pbPassword";
            this.pbPassword.Padding = new System.Windows.Forms.Padding(22, 20, 22, 20);
            this.pbPassword.Size = new System.Drawing.Size(63, 63);
            this.pbPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPassword.TabIndex = 2;
            this.pbPassword.TabStop = false;
            // 
            // tbPassword
            // 
            this.tbPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbPassword.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPassword.Location = new System.Drawing.Point(64, 9);
            this.tbPassword.MaxLength = 100;
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(402, 44);
            this.tbPassword.TabIndex = 2;
            this.tbPassword.WordWrap = false;
            // 
            // pnUsername
            // 
            this.pnUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnUsername.Controls.Add(this.pbUsername);
            this.pnUsername.Controls.Add(this.tbUsername);
            this.pnUsername.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnUsername.Location = new System.Drawing.Point(87, 355);
            this.pnUsername.Name = "pnUsername";
            this.pnUsername.Size = new System.Drawing.Size(472, 65);
            this.pnUsername.TabIndex = 5;
            // 
            // pbUsername
            // 
            this.pbUsername.Dock = System.Windows.Forms.DockStyle.Left;
            this.pbUsername.Image = global::Chickenfork_POS.Properties.Resources.logo_user;
            this.pbUsername.Location = new System.Drawing.Point(0, 0);
            this.pbUsername.Name = "pbUsername";
            this.pbUsername.Padding = new System.Windows.Forms.Padding(20);
            this.pbUsername.Size = new System.Drawing.Size(63, 63);
            this.pbUsername.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbUsername.TabIndex = 2;
            this.pbUsername.TabStop = false;
            // 
            // tbUsername
            // 
            this.tbUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbUsername.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUsername.Location = new System.Drawing.Point(64, 9);
            this.tbUsername.MaxLength = 100;
            this.tbUsername.Name = "tbUsername";
            this.tbUsername.Size = new System.Drawing.Size(402, 44);
            this.tbUsername.TabIndex = 1;
            this.tbUsername.WordWrap = false;
            // 
            // lbSilahkan
            // 
            this.lbSilahkan.AutoSize = true;
            this.lbSilahkan.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSilahkan.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbSilahkan.Location = new System.Drawing.Point(147, 292);
            this.lbSilahkan.Name = "lbSilahkan";
            this.lbSilahkan.Size = new System.Drawing.Size(350, 35);
            this.lbSilahkan.TabIndex = 4;
            this.lbSilahkan.Text = "Silahkan login terlebih dahulu";
            this.lbSilahkan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbWelcome
            // 
            this.lbWelcome.AutoSize = true;
            this.lbWelcome.Font = new System.Drawing.Font("Calibri", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbWelcome.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbWelcome.Location = new System.Drawing.Point(200, 237);
            this.lbWelcome.Name = "lbWelcome";
            this.lbWelcome.Size = new System.Drawing.Size(244, 64);
            this.lbWelcome.TabIndex = 3;
            this.lbWelcome.Text = "Welcome.";
            this.lbWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // logo_ayam
            // 
            this.logo_ayam.Image = global::Chickenfork_POS.Properties.Resources.logo_ayam;
            this.logo_ayam.Location = new System.Drawing.Point(243, 6);
            this.logo_ayam.Name = "logo_ayam";
            this.logo_ayam.Size = new System.Drawing.Size(180, 228);
            this.logo_ayam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logo_ayam.TabIndex = 0;
            this.logo_ayam.TabStop = false;
            // 
            // lnKeluar
            // 
            this.lnKeluar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lnKeluar.AutoSize = true;
            this.lnKeluar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnKeluar.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lnKeluar.LinkColor = System.Drawing.SystemColors.ControlDark;
            this.lnKeluar.Location = new System.Drawing.Point(422, 35);
            this.lnKeluar.Name = "lnKeluar";
            this.lnKeluar.Size = new System.Drawing.Size(218, 29);
            this.lnKeluar.TabIndex = 5;
            this.lnKeluar.TabStop = true;
            this.lnKeluar.Text = "Keluar dari aplikasi";
            this.lnKeluar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnKeluar_LinkClicked);
            // 
            // pnBottom
            // 
            this.pnBottom.Controls.Add(this.lnKeluar);
            this.pnBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnBottom.Location = new System.Drawing.Point(0, 987);
            this.pnBottom.Name = "pnBottom";
            this.pnBottom.Size = new System.Drawing.Size(1062, 97);
            this.pnBottom.TabIndex = 9;
            // 
            // pnContainer
            // 
            this.pnContainer.Controls.Add(this.pnBottom);
            this.pnContainer.Controls.Add(this.pnCenter);
            this.pnContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContainer.Location = new System.Drawing.Point(0, 0);
            this.pnContainer.Name = "pnContainer";
            this.pnContainer.Size = new System.Drawing.Size(1062, 1084);
            this.pnContainer.TabIndex = 10;
            // 
            // Form_Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.CancelButton = this.lnKeluar;
            this.ClientSize = new System.Drawing.Size(1062, 1084);
            this.ControlBox = false;
            this.Controls.Add(this.pnContainer);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(646, 726);
            this.Name = "Form_Login";
            this.ShowIcon = false;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnCenter.ResumeLayout(false);
            this.pnCenter.PerformLayout();
            this.pnPassword.ResumeLayout(false);
            this.pnPassword.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPassword)).EndInit();
            this.pnUsername.ResumeLayout(false);
            this.pnUsername.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbUsername)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo_ayam)).EndInit();
            this.pnBottom.ResumeLayout(false);
            this.pnBottom.PerformLayout();
            this.pnContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox logo_ayam;
        private System.Windows.Forms.Panel pnCenter;
        private System.Windows.Forms.Label lbSilahkan;
        private System.Windows.Forms.Label lbWelcome;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.Panel pnUsername;
        private System.Windows.Forms.PictureBox pbUsername;
        private System.Windows.Forms.Panel pnPassword;
        private System.Windows.Forms.PictureBox pbPassword;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.LinkLabel lnKeluar;
        private System.Windows.Forms.Panel pnBottom;
        private MetroFramework.Controls.MetroCheckBox cbAdmin;
        private MaterialSkin.Controls.MaterialRaisedButton btLogin;
        private System.Windows.Forms.Panel pnContainer;
    }
}

