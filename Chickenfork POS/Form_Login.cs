﻿using System;
using System.Windows.Forms;
using MaterialSkin.Controls;
using MaterialSkin;
using System.IO;

namespace Chickenfork_POS
{
    public partial class Form_Login : MaterialForm
    {
        #region Deklarasi variabel
        //Menyimpan nama username yang login
        public String usernameAkun = "";
        #endregion

        #region Constructor
        public Form_Login()
        {
            //Material Skin Form setting
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Red700, Primary.Red700, Primary.Red700, Accent.Red400, TextShade.WHITE);

            //Inisialisasi komponen
            InitializeComponent();

            //Cek direktori tempat menyimpan gambar
            string resourcePath = Path.GetDirectoryName(Application.ExecutablePath) + @"\res";
            if (!Directory.Exists(resourcePath))
            {
                Directory.CreateDirectory(resourcePath);
            }
        }
        #endregion

        #region Event handler method
        //Event handler link "Keluar dari aplikasi"
        private void lnKeluar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
        }

        //Event handler button "Login"
        private void btLogin_Click(object sender, EventArgs e)
        {
            this.usernameAkun = Connection.login(tbUsername.Text, tbPassword.Text, cbAdmin.Checked);
            if (usernameAkun != null)
            {
                if (cbAdmin.Checked)
                {
                    new Dashboard_Admin(this).ShowDialog();
                }
                else
                {
                    new Dashboard_Cashier(this).ShowDialog();
                }
            }
        }
        #endregion
    }
}
