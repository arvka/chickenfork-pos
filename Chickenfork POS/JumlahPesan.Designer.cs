﻿namespace Chickenfork_POS
{
    partial class JumlahPesan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btAdd = new MaterialSkin.Controls.MaterialFlatButton();
            this.btSub = new MaterialSkin.Controls.MaterialFlatButton();
            this.btSelesai = new MaterialSkin.Controls.MaterialFlatButton();
            this.btBatal = new MaterialSkin.Controls.MaterialFlatButton();
            this.pnCenter = new System.Windows.Forms.Panel();
            this.tbNamaMenu = new System.Windows.Forms.TextBox();
            this.pnJumlah = new System.Windows.Forms.Panel();
            this.tbJumlah = new System.Windows.Forms.TextBox();
            this.pnCenter.SuspendLayout();
            this.pnJumlah.SuspendLayout();
            this.SuspendLayout();
            // 
            // btAdd
            // 
            this.btAdd.AutoSize = true;
            this.btAdd.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btAdd.Depth = 0;
            this.btAdd.Icon = null;
            this.btAdd.Location = new System.Drawing.Point(294, -1);
            this.btAdd.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btAdd.MinimumSize = new System.Drawing.Size(95, 67);
            this.btAdd.MouseState = MaterialSkin.MouseState.HOVER;
            this.btAdd.Name = "btAdd";
            this.btAdd.Primary = false;
            this.btAdd.Size = new System.Drawing.Size(95, 67);
            this.btAdd.TabIndex = 1;
            this.btAdd.Text = "+";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btSub
            // 
            this.btSub.AutoSize = true;
            this.btSub.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSub.Depth = 0;
            this.btSub.Icon = null;
            this.btSub.Location = new System.Drawing.Point(-3, -1);
            this.btSub.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btSub.MinimumSize = new System.Drawing.Size(95, 67);
            this.btSub.MouseState = MaterialSkin.MouseState.HOVER;
            this.btSub.Name = "btSub";
            this.btSub.Primary = false;
            this.btSub.Size = new System.Drawing.Size(95, 67);
            this.btSub.TabIndex = 2;
            this.btSub.Text = "-";
            this.btSub.UseVisualStyleBackColor = true;
            this.btSub.Click += new System.EventHandler(this.btSub_Click);
            // 
            // btSelesai
            // 
            this.btSelesai.AutoSize = true;
            this.btSelesai.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btSelesai.Depth = 0;
            this.btSelesai.Icon = null;
            this.btSelesai.Location = new System.Drawing.Point(343, 199);
            this.btSelesai.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btSelesai.MouseState = MaterialSkin.MouseState.HOVER;
            this.btSelesai.Name = "btSelesai";
            this.btSelesai.Primary = false;
            this.btSelesai.Size = new System.Drawing.Size(102, 36);
            this.btSelesai.TabIndex = 4;
            this.btSelesai.Text = "Selesai";
            this.btSelesai.UseVisualStyleBackColor = true;
            this.btSelesai.Click += new System.EventHandler(this.btSelesai_Click);
            // 
            // btBatal
            // 
            this.btBatal.AutoSize = true;
            this.btBatal.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btBatal.Depth = 0;
            this.btBatal.Icon = null;
            this.btBatal.Location = new System.Drawing.Point(50, 199);
            this.btBatal.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btBatal.MouseState = MaterialSkin.MouseState.HOVER;
            this.btBatal.Name = "btBatal";
            this.btBatal.Primary = false;
            this.btBatal.Size = new System.Drawing.Size(87, 36);
            this.btBatal.TabIndex = 5;
            this.btBatal.Text = "Batal";
            this.btBatal.UseVisualStyleBackColor = true;
            this.btBatal.Click += new System.EventHandler(this.btBatal_Click);
            // 
            // pnCenter
            // 
            this.pnCenter.BackColor = System.Drawing.Color.White;
            this.pnCenter.Controls.Add(this.tbNamaMenu);
            this.pnCenter.Controls.Add(this.pnJumlah);
            this.pnCenter.Controls.Add(this.btBatal);
            this.pnCenter.Controls.Add(this.btSelesai);
            this.pnCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnCenter.Location = new System.Drawing.Point(7, 7);
            this.pnCenter.Name = "pnCenter";
            this.pnCenter.Size = new System.Drawing.Size(491, 276);
            this.pnCenter.TabIndex = 7;
            // 
            // tbNamaMenu
            // 
            this.tbNamaMenu.BackColor = System.Drawing.Color.White;
            this.tbNamaMenu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbNamaMenu.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNamaMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.tbNamaMenu.Location = new System.Drawing.Point(50, 30);
            this.tbNamaMenu.MaxLength = 4;
            this.tbNamaMenu.Name = "tbNamaMenu";
            this.tbNamaMenu.ReadOnly = true;
            this.tbNamaMenu.Size = new System.Drawing.Size(391, 35);
            this.tbNamaMenu.TabIndex = 8;
            this.tbNamaMenu.TabStop = false;
            this.tbNamaMenu.Text = "Menu Makanan";
            this.tbNamaMenu.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnJumlah
            // 
            this.pnJumlah.AutoSize = true;
            this.pnJumlah.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnJumlah.Controls.Add(this.tbJumlah);
            this.pnJumlah.Controls.Add(this.btSub);
            this.pnJumlah.Controls.Add(this.btAdd);
            this.pnJumlah.Location = new System.Drawing.Point(50, 96);
            this.pnJumlah.Name = "pnJumlah";
            this.pnJumlah.Size = new System.Drawing.Size(391, 67);
            this.pnJumlah.TabIndex = 7;
            // 
            // tbJumlah
            // 
            this.tbJumlah.BackColor = System.Drawing.Color.White;
            this.tbJumlah.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbJumlah.Font = new System.Drawing.Font("Calibri", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbJumlah.Location = new System.Drawing.Point(103, 8);
            this.tbJumlah.MaxLength = 4;
            this.tbJumlah.Name = "tbJumlah";
            this.tbJumlah.ReadOnly = true;
            this.tbJumlah.Size = new System.Drawing.Size(182, 49);
            this.tbJumlah.TabIndex = 3;
            this.tbJumlah.Text = "0";
            this.tbJumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbJumlah.WordWrap = false;
            // 
            // JumlahPesan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(505, 290);
            this.Controls.Add(this.pnCenter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "JumlahPesan";
            this.Padding = new System.Windows.Forms.Padding(7);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "JumlahPesan";
            this.pnCenter.ResumeLayout(false);
            this.pnCenter.PerformLayout();
            this.pnJumlah.ResumeLayout(false);
            this.pnJumlah.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MaterialSkin.Controls.MaterialFlatButton btAdd;
        private MaterialSkin.Controls.MaterialFlatButton btSub;
        private MaterialSkin.Controls.MaterialFlatButton btSelesai;
        private MaterialSkin.Controls.MaterialFlatButton btBatal;
        private System.Windows.Forms.Panel pnCenter;
        private System.Windows.Forms.Panel pnJumlah;
        private System.Windows.Forms.TextBox tbJumlah;
        private System.Windows.Forms.TextBox tbNamaMenu;
    }
}