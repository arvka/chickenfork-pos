﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Chickenfork_POS
{
    public partial class JumlahPesan : Form
    {
        #region Deklarasi variabel
        private Dashboard_Cashier dc;
        private string nama_item, harga_item, jenis_item;
        private int jumlah;
        #endregion

        #region Constructor
        public JumlahPesan(Dashboard_Cashier _parent)
        {
            InitializeComponent();
    
            jumlah = 0;
            nama_item = harga_item = jenis_item = "";
            dc = _parent;
        }
        #endregion

        #region User method
        public void setVariabel(string nama, string harga, string jenis)
        {
            this.nama_item = nama;
            this.harga_item = harga;
            this.jenis_item = jenis;
            this.tbNamaMenu.Text = nama;
        }
        #endregion

        #region Event handler method
        private void btAdd_Click(object sender, EventArgs e)
        {
            try
            {
                jumlah = Int32.Parse(tbJumlah.Text);
                tbJumlah.Text = (++jumlah).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan Input");
            }
        }

        private void btSub_Click(object sender, EventArgs e)
        {
            try
            {
                jumlah = Int32.Parse(tbJumlah.Text);
                if (jumlah > 0)
                {
                    tbJumlah.Text = (--jumlah).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Terjadi Kesalahan Input");
            }
        }

        private void btBatal_Click(object sender, EventArgs e)
        {
            this.tbJumlah.Text = "0";
            this.Hide();
        }

        private void btSelesai_Click(object sender, EventArgs e)
        {
            //Membuat baris
            DataRow dr = dc.dt.NewRow();
            dr["Menu"] = this.nama_item;
            dr["Jenis"] = this.jenis_item;
            dr["Jumlah"] = this.jumlah;
            dr["Harga/porsi"] = this.harga_item;
            dr["Sub Total"] = jumlah * Int32.Parse(harga_item);

            //Menambahkan baris ke DataTable
            dc.dt.Rows.Add(dr);

            //Update total harga
            dc.hitungTotalHarga();
            dc.currentRow++;

            //Reset jumlah harga sebelumnya
            this.tbJumlah.Text = "0";

            this.Hide();
        }
        #endregion
    }
}
