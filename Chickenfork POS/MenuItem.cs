﻿using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;

namespace Chickenfork_POS
{
    class MenuItem : Button
    {
        private string thumb;
        private Dashboard_Cashier parent;
        public string nama, harga, jenis;
        public event EventHandler ButtonClick;

        public MenuItem(ImageList _MenuItemList, string _nama, string _harga, string _thumb, string _jenis)
        {
            Initialize();

            this.nama = _nama;
            this.harga = _harga;
            this.jenis = _jenis;

            if (_thumb == "")
            {
                if (_jenis == "staff")
                {
                    this.ImageIndex = 1;
                }
                else
                {
                    this.ImageIndex = 0;
                }
            }
            else
            {
                string path = Path.GetDirectoryName(Application.ExecutablePath) + @"\res\"+_thumb;
                _MenuItemList.Images.Add(_thumb, Image.FromFile(path));
                this.Name = _nama.Replace(" ", "_");
                this.ImageKey = _thumb;
            }
            
            this.ImageList = _MenuItemList;
            this.Text = _nama + Environment.NewLine + "[ " + _harga + " ]";
        }

        private void Initialize()
        {
            this.BackColor = System.Drawing.Color.White;
            this.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.FlatAppearance.BorderSize = 3;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            //this.Location = new System.Drawing.Point(3, 3);
            this.Size = new System.Drawing.Size(170, 175);
            this.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.UseVisualStyleBackColor = false;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            this.BackColor = System.Drawing.Color.White;
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            this.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(54)))), ((int)(((byte)(40)))));
        }

        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            this.FlatAppearance.BorderColor = System.Drawing.Color.White;
            if (this.ButtonClick != null)
            {
                this.ButtonClick(this, mevent);
            }
        }
    }
}
